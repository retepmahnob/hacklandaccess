#ifndef _LOCK_H_
#define _LOCK_H_

#define MAX_LOCK_TIME (24 * 3600)                   // The maximum allowed lock time in seconds
#define UNLOCK_TIME_WHEN_BUTTON_PRESSED_SECONDS (5) // How long to unlock the lock when the exit button is pressed
#define DELAY_REPORT_DOOR_OPEN (30)

#define UNLOCK_BUTTON_PIN 5 // D1 => GPIO_05   The door release button - wire to ground via a physical button
#define RELAY_PIN 4         // D2 => GPIO_04   The door release relay
#define DOOR_OPEN_PIN 2     // D4 => GPIO_02

#endif // _LOCK_H_
