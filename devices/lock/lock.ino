#include <SPI.h>

#define VERSION String("1.1.0")

#include <ESP8266WiFi.h>
#include <PubSubClient.h> // See https://pubsubclient.knolleary.net/

//The following libraries must be installed into your arduino libraries folder
#include <SimpleTimer.h>    //from github, https://github.com/schinken/SimpleTimer, download the cpp and h files (raw) and place into a folder named "SimpleTimer" in the arduino libraries folder
#include <HacklandDevice.h> // This is the main device orchasitation code

#include "secrets.h" // This contains the "SSID", "PASSWORD" and "MQTT_SERVER" values.
#include "lock.h"    // Details of the lock, like PIN/GPIO numbers

#define DEVICE_TYPE "lock" // That's me

HacklandDevice *device = new HacklandDevice(DEVICE_TYPE);
SimpleTimer timer; //a timer object to handle periodic tasks without blocking delay()

const bool debug = false;
bool buttonPressed = false;
unsigned long unlockUntil = 0; // When to next set to locked state, 0 means in the past
int lockState = LOW;
int doorOpenTime = 0;

// the setup function runs once when you press reset or power the board
void setup()
{
    // Set everything up
    device->rebootInSeconds(2 * 3600);                   // We have discovered some issues, hope this helps.
    device->enableWifi(WIFI_SSID, WIFI_PASSWORD);        // Start Wifi
    device->enableRemoteDebug();                         // Start remote debugger (logs via MQTT)
    device->enableMqtt(MQTT_SERVER);                     // Start MQTT
    device->enableOta();                                 // Enable Over-The-Air updates
    device->onAction("unlock", &unlockResponseCallback); // Subscribe to unlock action messages
    device->setVersion(VERSION);

    initializeLock();

    timer.setInterval(100, timerLoop); // The timer for the main loop
}

void loop()
{
    timer.run();
}

void timerLoop()
{
    device->run();

    // Check if we need to lock or not.
    const unsigned long now = millis();
    if (unlockUntil > 0 && unlockUntil >= now)
    {
        // Unlock
        lockState = HIGH;
        if (debug)
        {
            device->println(String(now) + String(" Unlocked"));
        }
    }
    else
    {
        // Lock
        lockState = LOW;
        unlockUntil = 0;
    }
    digitalWrite(RELAY_PIN, lockState);
}

void initializeLock()
{
    // The unlock relay
    pinMode(RELAY_PIN, OUTPUT);

    // The door open detection pin
    pinMode(DOOR_OPEN_PIN, INPUT_PULLUP);
    timer.setInterval(1000, runTestDoorOpen);

    // The unlock button
    pinMode(UNLOCK_BUTTON_PIN, INPUT_PULLUP);
    timer.setInterval(20, runTestUnlockButton);
}

void runTestDoorOpen()
{
    bool doorOpen = (HIGH != digitalRead(DOOR_OPEN_PIN));
    if (doorOpen)
    {
        if (doorOpenTime == 0)
        {
            reportDoorState("open");
        }

        doorOpenTime += 1;
        if (doorOpenTime % DELAY_REPORT_DOOR_OPEN == 0)
        {
            reportDoorState("held-open");
        }
    }
    else if (doorOpenTime > 0)
    {
        reportDoorState("closed");
        doorOpenTime = 0;
    }
}

void reportDoorState(const char *msg)
{
    device->publishEvent("state", msg);
}

// Read if a button is pressed.  This contains some simple debounce
// mechanism.
void runTestUnlockButton()
{
    bool buttonCurrentlyPressed = (HIGH != digitalRead(UNLOCK_BUTTON_PIN));
    if (buttonCurrentlyPressed && !buttonPressed)
    {
        device->println("Button pressed.");
        deviceUnlock(UNLOCK_TIME_WHEN_BUTTON_PRESSED_SECONDS);
    }
    buttonPressed = buttonCurrentlyPressed;
}

void deviceUnlock(int unlockTimeSecs)
{
    if (unlockTimeSecs <= 0)
        return;
    if (unlockTimeSecs >= MAX_LOCK_TIME)
        return;

    unlockUntil = (unlockTimeSecs * 1000) + millis();

    device->print("UNLOCK until: ");
    device->println(String(unlockUntil));
}

void unlockResponseCallback(const char *unlockTime)
{
    device->print("Unlock time: ");
    device->println(unlockTime);

    int unlockTimeSecs = String(unlockTime).toInt();

    deviceUnlock(unlockTimeSecs);
}
