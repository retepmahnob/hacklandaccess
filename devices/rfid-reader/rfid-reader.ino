#include <SPI.h>

#define VERSION String("1.1.1")

//The following libraries must be installed into your arduino libraries folder
#include <MFRC522.h>        // From the library manager, search for frc and install MFRC522 By GithubCommunity Version 1.4.3
#include <SimpleTimer.h>    // From github, https://github.com/schinken/SimpleTimer, download the cpp and h files (raw) and place into a folder named "SimpleTimer" in the arduino libraries folder
#include <jled.h>           // The LED library
#include <HacklandDevice.h> // This is the main device orchasitation code

#include "secrets.h" // This contains the "SSID", "PASSWORD" and "MQTT_SERVER" values.

// RFID reader config
#define SS_PIN 15              // D8 => GPIO 15
#define RST_PIN 0              // D3 => GPIO 0
#define LED_PIN D1             // ON nodeMCU
#define BUZ_PIN D2             // ON nodeMCU
#define CARD_READ_INTERVAL 200 // How often (milliseconds) to check for a new RFID card

#define DEVICE_TYPE "rfid-reader" // That's this device

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance; // Create MFRC522 instance
SimpleTimer timer;                // a timer object to handle periodic tasks without blocking delay()
auto timerIntervalMs = 10;

auto ledIndicator = JLed(LED_PIN).Blink(250, 1750).Forever();

// A buzzer works just like an LED
auto buzzer = JLed(BUZ_PIN).Stop();

const bool debugRfid = false;
HacklandDevice *device = new HacklandDevice(DEVICE_TYPE);

// the setup function runs once when you press reset or power the board
void setup()
{

    // Set everything up
    device->rebootInSeconds(2 * 3600);               // We have discovered some issues, hope this helps.
    device->enableWifi(WIFI_SSID, WIFI_PASSWORD);    // Start Wifi
    device->enableRemoteDebug();                     // Start remote debugger (logs via MQTT)
    device->enableMqtt(MQTT_SERVER);                 // Start MQTT
    device->enableOta();                             // Enable Over-The-Air updates
    device->onAction("auth", &authResponseCallback); // Subscribe to authorisation messages
    device->onAction("buzzer", &buzzerCallback);     // Subscribe to buzzer on/off messages
    device->setVersion(VERSION);

    timer.setInterval(timerIntervalMs, timerLoop); // The timer for the main loop

    initializeRfidReader(); // Now we can start the RFID reader
    initializeLED();        // ... and the LEDs for the RFID reader
    initializeBuzzer();     // ... and the buzzer for when the door is open
}

void loop()
{
    timer.run();
}

void timerLoop()
{
    device->run();
}

void initializeRfidReader()
{
    Serial.println(F("Init rfid reader"));
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522

    if (debugRfid)
    {
        mfrc522.PCD_DumpVersionToSerial(); // Show details of PCD - MFRC522 Card Reader details
        Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
    }
    timer.setInterval(CARD_READ_INTERVAL, readRfidToSerial);
}

void initializeLED()
{
    timer.setInterval(timerIntervalMs, runLEDs);
}

void runLEDs()
{
    if (!ledIndicator.IsRunning())
    {
        jledWaiting();
    }
    ledIndicator.Update();
}

void readRfidToSerial()
{

    if (!mfrc522.PICC_IsNewCardPresent())
    {
        if (debugRfid)
        {
            device->println("No card present");
        }
        return;
    }

    // Update indicator LED to show that a card is present
    jledCardRead();

    if (debugRfid)
    {
        device->println("Card present");
    }

    // Select one of the cards
    if (!mfrc522.PICC_ReadCardSerial())
    {
        if (debugRfid)
        {
            device->println("Failed to select card");
        }
        return;
    }

    // Dump debug info about the card; PICC_HaltA() is automatically called
    //   mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

    String rfidUIDValue = "";
    for (byte i = 0; i < mfrc522.uid.size; i++)
    {
        rfidUIDValue.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
        rfidUIDValue.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    rfidUIDValue.toUpperCase();
    device->publishEvent("card", rfidUIDValue.c_str());

    if (debugRfid)
    {
        device->print("UID tag:");
        device->println(rfidUIDValue);
    }
}

void authResponseCallback(const char *authResponse)
{
    device->print("Authorisation response: ");
    device->println(authResponse);

    if (String(authResponse) == String("success"))
    {
        device->println("Accepted");
        jledCardAccepted();
        buzzerBeepSuccess();
    }
    else if (String(authResponse) == String("failure"))
    {
        device->println("Rejected");
        jledCardRejected();
        buzzerBeepError();
    }
    else
    {
        device->println("calback failed");
        buzzerBeepError();
    }
}

void jledWaiting()
{
    ledIndicator.Stop();
    ledIndicator.Blink(250, 1750).Repeat(10);
}

void jledCardRead()
{
    ledIndicator.Stop();
    ledIndicator.Blink(500, 500).Repeat(2);
}

void jledCardAccepted()
{
    ledIndicator.Stop();
    ledIndicator.On().DelayAfter(5000);
}

void jledCardRejected()
{
    ledIndicator.Stop();
    ledIndicator.Blink(100, 100).Repeat(10);
}

void initializeBuzzer()
{
    timer.setInterval(timerIntervalMs, runBuzzer);
}

void runBuzzer()
{
    buzzer.Update();
}

void buzzerCallback(const char *buzzerState)
{
    device->print("Buzzer state: ");
    device->println(buzzerState);

    if (String(buzzerState) == String("on"))
    {
        buzzerOn();
    }
    else if (String(buzzerState) == String("off"))
    {
        buzzerOff();
    }
    else
    {
        device->println("calback failed");
    }
}

void buzzerBeepSuccess()
{
    buzzer.Stop();
    buzzer.Blink(100, 120).Repeat(1);
}

void buzzerBeepError()
{
    buzzer.Stop();
    buzzer.Blink(70, 120).Repeat(3);
}

void buzzerOn()
{
    if (!buzzer.IsRunning())
    {
        buzzer.Blink(250, 1250).Forever();
    }
}

void buzzerOff()
{
    buzzer.Stop();
}
