
#include "HacklandDevice.h"

#if defined ESP8266 || defined ESP32
#else
#error "The board must be ESP8266 or ESP32"
#endif // ESP

const bool debugMQTT = false;
bool initialised = false;

void (*resetFunc)(void) = 0;

// Check if we need to reboot
void checkReboot(unsigned long rebootTimeMs)
{
    if (rebootTimeMs > 0 && millis() >= rebootTimeMs)
    {
        Serial.println("Rebooting");
        resetFunc();
    }
}

HacklandDevice::HacklandDevice(const char type[])
{
    if (initialised)
    {
        Serial.println("ERROR: HacklandDevice is already initalised, only use one entity");
        return;
    }
    initialised = true;

    _isFirstConnection = true;
    _remoteLog = false;
    _printString = String();
    _version = String();
    _rebootTimeMs = 0;

    if (strlen(type) + 1 > sizeof(_type))
    {
        Serial.println("ERROR: Device type is too long");
        strcpy(_type, "UNKNOWN");
    }
    else
    {
        strcpy(_type, type);
    }

    mqttClient = PubSubClient(espClient);

    initSerial();
}

void HacklandDevice::initSerial()
{
    Serial.begin(115200);
    while (!Serial)
    {
        ; // wait for serial attach
        checkReboot(_rebootTimeMs);
    }
}

void HacklandDevice::rebootInSeconds(long rebootTimeSec)
{
    _rebootTimeMs = rebootTimeSec * 1000;
}

void HacklandDevice::run()
{

    checkReboot(_rebootTimeMs);

    //
    // MQTT
    //
    if (!mqttClient.connected())
    {
        reconnectMqtt();
    }
    else
    {
        if (_isFirstConnection)
        {
            _isFirstConnection = false;
            if (_version.length() > 0)
            {
                publishEvent("version", _version.c_str());
            }
        }
    }
    mqttClient.loop();
}

void HacklandDevice::setVersion(String version)
{
    _version = version;
}

//
//
//      Wifi code
//
//

void HacklandDevice::enableWifi(const char ssid[], const char password[])
{

    delay(100);

    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    // Required for OTA
    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP(ssid, password);

    while (WiFiMulti.run() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        checkReboot(_rebootTimeMs);
    }
    randomSeed(micros());

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void getMac(char mac[13])
{
    String s = WiFi.macAddress();
    for (int i = 0, j = 0; i < 12; i += 1)
    {
        mac[i] = s.charAt(j);
        if (s.charAt(j + 1) == ':')
        {
            j += 2;
        }
        else
        {
            j += 1;
        }
    }
    mac[12] = 0x00;
}

//
//
//      MQTT code
//
//

void callbackH(char *topic, byte *payload, unsigned int length)
{
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (int i = 0; i < length; i++)
    {
        Serial.print((char)payload[i]);
    }
    Serial.println();
}

void HacklandDevice::enableMqtt(const char mqttServer[])
{
    strcpy(_server, mqttServer);
    mqttClient.setServer(_server, 1883);
    mqttClient.setCallback([this](char *topic, byte *payload, unsigned int length) {
        this->callbackHandler(topic, payload, length);
    });
}

void HacklandDevice::reconnectMqtt()
{
    // Loop until we're reconnected
    while (!mqttClient.connected())
    {
        Serial.print("Attempting MQTT connection...");
        // Create a random client ID
        String clientId = "ESP8266Client-";
        clientId += String(random(0xffff), HEX);
        // Attempt to connect
        if (mqttClient.connect(clientId.c_str()))
        {
            Serial.println("connected");
            // Register to the server
            publishEvent("register", _type);
            subscribeAllTopics();
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(mqttClient.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
        checkReboot(_rebootTimeMs);
    }
}

bool createTopic(char output_topic[], const int output_topic_len, const char cmd[])
{
    char mac[13];
    getMac(mac);
    return snprintf(output_topic, output_topic_len, "/device/%s/%s", mac, cmd) > 0;
}

void HacklandDevice::publishEvent(const char event[], const char message[])
{
    char pub_topic[MAX_TOPIC_LENGTH + 1];
    if (!createTopic(pub_topic, MAX_TOPIC_LENGTH, event))
        return;

    mqttClient.publish(pub_topic, message);

    if (debugMQTT)
    {
        Serial.print("Published '");
        Serial.print(message);
        Serial.print("' to topic: ");
        Serial.println(pub_topic);
    }
}

template <class T>
void HacklandDevice::onAction(const char *action, T *const object, void (T::*const mf)(const char *))
{
    using namespace std::placeholders;
    _callbacks.emplace_back(std::bind(mf, object, _1));

    char topic[MAX_TOPIC_LENGTH + 1];
    if (!createTopic(topic, MAX_TOPIC_LENGTH, action))
    {
        Serial.println("ERROR: There was an error creating the topic name");
        return;
    }
    _subTopics.emplace_back(String(topic));
}

void HacklandDevice::onAction(const char *action, void (*const fun)(const char *))
{
    _callbacks.emplace_back(fun);

    char topic[MAX_TOPIC_LENGTH + 1];
    if (!createTopic(topic, MAX_TOPIC_LENGTH, action))
    {
        Serial.println("ERROR: There was an error creating the topic name");
        return;
    }
    _subTopics.emplace_back(String(topic));
}

onActionCallback_t HacklandDevice::getSubId(String topic)
{
    int i = 0;
    for (const auto &st : _subTopics)
    {
        if (st == topic)
        {
            return _callbacks[i];
        }
        i += 1;
    }

    return NULL;
}

void HacklandDevice::subscribeAllTopics()
{

    for (const auto &st : _subTopics)
    {
        Serial.print("Subscribing: ");
        Serial.println(st);
        mqttClient.subscribe(st.c_str());
    }
}

void HacklandDevice::callbackHandler(char *topic, byte *payload, unsigned int length)
{

    Serial.print("Message arrived on '");
    Serial.print(topic);
    Serial.print("' -> ");
    for (int i = 0; i < length; i++)
    {
        Serial.print((char)payload[i]);
    }
    Serial.println();

    auto callback = getSubId(topic);
    if (callback == NULL)
    {
        Serial.print("We are not subscribed to this topic: ");
        Serial.println(topic);
        return;
    }

    if (length > MAX_TOPIC_DATA_LEN)
    {
        Serial.println("Invalid message length arrived.");
        return;
    }

    // Convert the payload bytes to a string (we assume all our messages are strings)
    char payload_str[MAX_TOPIC_DATA_LEN + 1];
    int i;
    for (i = 0; i < length; i += 1)
    {
        payload_str[i] = payload[i];
    }
    payload_str[i] = '\0';

    // Now retreive the callback and run it
    callback(payload_str);
}

//
//
//      Over-the-air (OTA) update code
//
//

void HacklandDevice::enableOta()
{
    onAction("upgrade", this, &HacklandDevice::doOtaUpgrade);
}

void HacklandDevice::doOtaUpgrade(const char *otaFilename)
{
    Serial.print("Doing OTA upgrade with file: ");
    Serial.println(otaFilename);

    WiFiClient client;
    const int url_len = 128;
    char url[url_len + 1];
    auto result = snprintf(url, url_len, "http://%s:8088/%s", _server, otaFilename);

    if (result <= 0)
    {
        return;
    }

    t_httpUpdate_return ret = ESPhttpUpdate.update(client, url);

    switch (ret)
    {
    case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;

    case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

    case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
}

void update_started()
{
    Serial.println("CALLBACK:  HTTP update process started");
}

void update_finished()
{
    Serial.println("CALLBACK:  HTTP update process finished");
}

void update_progress(int cur, int total)
{
    Serial.printf("CALLBACK:  HTTP update process at %d of %d bytes...\n", cur, total);
}

void update_error(int err)
{
    Serial.printf("CALLBACK:  HTTP update fatal error code %d\n", err);
}

//
//
//      Over-the-air debugging
//
//

void HacklandDevice::enableRemoteDebug()
{
    _remoteLog = true;
}

void HacklandDevice::print(const char str[])
{
    print(String(str));
}

void HacklandDevice::println(const char str[])
{
    println(String(str));
}

void HacklandDevice::print(String str)
{
    if (_remoteLog)
    {
        _printString += str;
    }
    Serial.print(str);
    Serial.flush();
}

void HacklandDevice::println(String str)
{
    _printString += str;
    if (_remoteLog)
    {
        publishEvent("log", _printString.c_str());
    }
    Serial.println(_printString);
    Serial.flush();
    _printString = String();
}
