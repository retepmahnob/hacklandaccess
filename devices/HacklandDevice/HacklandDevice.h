#ifndef HACKLAND_DEVICE_H
#define HACKLAND_DEVICE_H

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#include <PubSubClient.h> // See https://pubsubclient.knolleary.net/

//typedef void (*on_action_callback_t)(char *message);
typedef std::function<void(const char *)> onActionCallback_t;

#define SERVER_LEN 64

class HacklandDevice
{
public:
    HacklandDevice(const char type[]);

    //
    // Setup functions
    //

    /*!
     * @breif Enable the Wifi for the ESP8266 module.
     * @param ssid The name of the Wifi connection
     * @param password The Wifi's password
     */
    void enableWifi(const char ssid[], const char password[]);

    /*!
     * @breif Enable the MQTT and set the server.
     * @param mqttServer The IP address of MQTT server.
     */
    void enableMqtt(const char mqttServer[]);

    /*!
     * @breif Enable OTA (Over-the-air) updates.  This will be managed by the MQTT session.
     */
    void enableOta();

    /*!
     * @breif After this many seconds of uptime the device will reboot.  Set to zero to
     */
    void rebootInSeconds(long rebootTimeSec);

    /*!
     * @breif This is the main running function, it must be called during each loop.
     */
    void run();

    /*!
     * @breif Enable the MQTT and set the server.
     * @param event The event topic to publish to.
     * @param message The MQTT message to send.
     */
    void publishEvent(const char event[], const char message[]);

    /*!
     * @breif When an action comes from the server, let's handle it by executing the callback.
     * @param action The name of the action to handle.
     * @param object The class instance.
     * @param callback The callback function that runs when an action is received.
     */
    template <class T>
    void onAction(const char *action, T *const object, void (T::*const callback)(const char *));

    /*!
     * @breif When an action comes from the server, let's handle it by executing the callback.
     * @param action The name of the action to handle.
     * @param callback The callback function that runs when an action is received.
     */
    void onAction(const char *action, void (*const callback)(const char *));

    /*!
     * @breif Enables remote debug.  You can then Telnet to the IP address to get the logs.
     */
    void enableRemoteDebug();
    void print(const char str[]);
    void println(const char str[]);
    void print(const String str);
    void println(const String str);

    /*!
     * @breif Set the version number.
     */
    void setVersion(String);

private:
    // maximum number of mqty subscriptions
    const static int MAX_MQTT_SUBS = 5;
    const static int MAX_TOPIC_LENGTH = 128;
    const static int MAX_TOPIC_DATA_LEN = 256;

    // Variables
    char _type[32];
    WiFiClient espClient;
    ESP8266WiFiMulti WiFiMulti;
    PubSubClient mqttClient;
    char _server[SERVER_LEN];
    String _printString;
    String _version;
    bool _isFirstConnection;
    unsigned long _rebootTimeMs;

    void initSerial();
    void reconnectMqtt();
    void publishRegistration();

    void callbackHandler(char *topic, byte *payload, unsigned int length);
    void subscribeAllTopics();
    onActionCallback_t getSubId(String topic);

    // OTA
    void doOtaUpgrade(const char *otaFilename);

    // The callback functions for the topics that we subscribe to.
    std::vector<onActionCallback_t> _callbacks;
    std::vector<String> _subTopics;

    // True when remote debugging is enabled
    bool _remoteLog;
};

#endif // HACKLAND_DEVICE_H
