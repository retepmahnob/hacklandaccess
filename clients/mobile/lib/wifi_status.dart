import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'bloc/wifi_bloc.dart';
import 'bloc/wifi_event.dart';
import 'bloc/wifi_state.dart';

class WifiStatus extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WifiStatusState();
}

class _WifiStatusState extends State<WifiStatus> {
  @override
  Widget build(BuildContext context) {
    return Container(child: BlocBuilder<WifiBloc, WifiState>(builder: (context, state) {
      if (state.CurrentState == WifiCommstate.Sacnning) {
        return Center(
          child: HeartbeatProgressIndicator(
            startScale: 1,
            endScale: 6,
            child: Icon(
              Icons.network_check,
              color: Colors.lightGreen,
            ),
          ),
        );
      }
      else if (state.CurrentState == WifiCommstate.Connected) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                (state.ssid != null) ? state.ssid : 'unknown SSID',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              ),
              Text(
                (state.ipAddress != null) ? state.ipAddress : 'unknown Ip',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              ),
              Text("Device Id"),
              Text(
                (state.identifier != null) ? state.identifier : 'unknown',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              ),
              ElevatedButton(
                onPressed: () {
                  context.read<WifiBloc>().add(WifiUnlockDoorEvent(doorName: 'hl1door'));
                },
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(0.0),
                  elevation: 6.0,
                  textStyle: TextStyle(
                    color: Colors.black,
                  )
                ),
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[
                        Color(0xebcf34),
                        Color(0xFF1976D2),
                        Color(0x5e5108),
                      ],
                    ),
                  ),
                  padding: const EdgeInsets.all(20.0),
                  child: const Text('Let Me in !!!', style: TextStyle(fontSize: 40)),
                ),
              ),
            ],
          ),
        );
      }
      return Center(
        child: Text(
          "Scanning",
          style: TextStyle(fontSize: 48),
        ),
      );
    }));
  }
}
