import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackcess/api/hackcess-api.dart';
import './monitor_bloc_delegate.dart';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import 'HacklandConfig.dart';
import 'access_control.dart';
import 'bloc/mqtt_bloc.dart';
import 'bloc/wifi_bloc.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final HacklandConfig hacklandConfig = HacklandConfig(
      defaultConfig:  HacklandConfigData(
          ipAddress: '192.168.0.9'
      )
  );

  var settings = await hacklandConfig.loadconfig();
  HackcessApi api = new HackcessApi(httpClient: http.Client(), hostIpAddress: settings.ipAddress);
  runApp(AccessApp(
    hackcessApi: api,
    hacklandConfig: hacklandConfig,
  ));
}

class AccessApp extends StatelessWidget {
  final HackcessApi hackcessApi;
  final HacklandConfig hacklandConfig;

  AccessApp ({Key? key, required this.hackcessApi, required this.hacklandConfig}) : super (key: key);

  @override
  Widget build(BuildContext context) {
    MqttBloc mqtt = MqttBloc();
    return MaterialApp(
      title: 'Hackland let me in !!!',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider<WifiBloc>(
            create: (context) => WifiBloc(hackcessApi, mqtt),
          ),
          BlocProvider<MqttBloc>(
            create: (context) => mqtt,
          )
        ],
        child: RepositoryProvider<HacklandConfig>(
          create: (context) => hacklandConfig,
            child: AccessControl(
            title: 'Hackland - Let me in !!'),
        ),
      ),
    );
  }
}
