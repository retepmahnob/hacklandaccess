import 'dart:convert';
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';

class HacklandConfigData {
  final String ipAddress;

  HacklandConfigData({required this.ipAddress});
  Map<String, dynamic> toJson() => {'ipAddress': ipAddress};

  HacklandConfigData.fromJson(Map<String, dynamic> json)
      : ipAddress = json['ipAddress'];

}

class HacklandConfig {

  final HacklandConfigData defaultConfig;
  HacklandConfigData? config;
  HacklandConfig({required this.defaultConfig});

  Future<HacklandConfigData> loadconfig() async {

    Directory dir = await getApplicationDocumentsDirectory();
    File localStorage = File('${dir.path}/Hackland.json');
    try {
      String js = await localStorage.readAsString();
      config = HacklandConfigData.fromJson(jsonDecode(js));
      return config!;
    } catch (FileSystemException) {
      debugPrint("No saved file Configuration");
    }
    return defaultConfig;
  }

  void saveConfig(HacklandConfigData save) async {
    var jsonConfig = jsonEncode(save.toJson());
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path;
    File localStorage = File('$path/Hackland.json');
    await localStorage.writeAsString(jsonConfig);
    return;
  }

}
