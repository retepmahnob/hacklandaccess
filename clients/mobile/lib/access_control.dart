import 'dart:convert';
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackcess/HacklandConfig.dart';
import 'package:hackcess/bloc/mqtt_bloc.dart';
import 'package:hackcess/wifi_status.dart';
import 'package:path_provider/path_provider.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'bloc/mqtt_event.dart';
import 'bloc/wifi_bloc.dart';
import 'mqtt_status.dart';

class AccessControl extends StatefulWidget {
  AccessControl({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _AccessControlState createState() => _AccessControlState();
}

class _AccessControlState extends State<AccessControl> {
  late TextEditingController ipAddressController;
  String originalIpAddress = '';
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    originalIpAddress =
        RepositoryProvider.of<HacklandConfig>(context).config!.ipAddress;
    ipAddressController = TextEditingController();
    ipAddressController.value = TextEditingValue(text: originalIpAddress);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/HacklandLogo.png"),
              fit: BoxFit.fill,
              alignment: Alignment.center,
              colorFilter: ColorFilter.mode(
                  Colors.white.withOpacity(0.95), BlendMode.dstATop))),
      child: Scaffold(
          onDrawerChanged: (bool isOpen) {
            if (!isOpen && _formKey.currentState!.validate()) {
              var data =
                  HacklandConfigData(ipAddress: ipAddressController.value.text);
              if (data.ipAddress != originalIpAddress) {
                context
                    .read<MqttBloc>()
                    .add(MqttWifiAvailableEvent(ipAddress: data.ipAddress));
                RepositoryProvider.of<HacklandConfig>(context).saveConfig(data);
              }
            }
          },
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text(widget.title),
          ),
          drawer: _buildDrawer(),
          body: Column(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Center(child: WifiStatus()),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: MqttStatus(),
                ),
              )
            ],
          )),
    );
  }

  Widget _buildDrawer() {
    return Column(
      children: [
        Expanded(
          child: Drawer(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountEmail: Text('retepmahnob@gmail.com'),
                      accountName: Text('Peter Bonham'),
                      currentAccountPicture: CircleAvatar(
                        backgroundImage: NetworkImage(
                            'https://api.adorable.io/avatars/285/abott@adorable.png'),
                      ),
                    ),
                  ]),
                ),
                Spacer(),
                Expanded(
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Form(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: TextFormField(
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            decoration: const InputDecoration(
                              icon: Icon(Icons.network_check_sharp),
                              hintText: 'ip address',
                            ),
                            controller: ipAddressController,
                            validator: (value) =>
                                (validator.ip(value!)) ? null : "Not valid",
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
      ],
    );
  }
}
