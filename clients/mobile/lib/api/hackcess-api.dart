import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class HackcessApi {
  final http.Client httpClient;
  final String hostIpAddress;
  HackcessApi({required this.httpClient, required this.hostIpAddress}) : assert(httpClient != null);

  Future<int> openDoor (String phoneId, String doorName) async {

    final request = await httpClient.post(Uri(host: 'http://$hostIpAddress:8088/api/device/mobile/$doorName/$phoneId/'),
        headers: {"Content-Type": "application/json"});
    /*,
        body: jsonEncode('{}}'));
*/
    return request.statusCode;
  }
}