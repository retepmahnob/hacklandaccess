
import 'package:mqtt_client/mqtt_client.dart' as mqtt;
import 'package:equatable/equatable.dart';

enum mqttCurrentState {
  WaitWifiState,
  Mqttconnected,
  DoorOpen
}

class DoorOpenState {
  String doorName = '';
  DateTime closes = DateTime.now();
}

class MqttState extends Equatable {  @override
  mqttCurrentState? currentState;
  List<DoorOpenState> openDoors = <DoorOpenState>[];

  MqttState () : currentState = mqttCurrentState.WaitWifiState;


  List<Object> get props => [];
}

class MqttWaitWifiState extends MqttState {
  @override
  List<Object> get props => [];
}

class MqttConnectingState extends MqttState {
  final String destAddress;
  MqttConnectingState({required this.destAddress});
  @override
  List<Object> get props => [];

}

class MqttChangeOfState extends MqttState {
  final mqtt.MqttConnectionState connectionState;

  MqttChangeOfState({required this.connectionState});
  @override
  List<Object> get props => [connectionState];
}

class MqttDisconnectedState extends MqttState {
  @override
  List<Object> get props => [];
}

class MqttUnlocDoorState extends MqttState {
  final String door;
  final int duration;

  MqttUnlocDoorState({required this.duration, required this.door});
  @override
  List<Object> get props => [door, duration];
}


class MqttDateReceivedState extends MqttState {
  final String payload;
  MqttDateReceivedState(this.payload);
  @override
  List<Object> get props => [payload];
}