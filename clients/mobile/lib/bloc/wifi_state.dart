import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

enum WifiCommstate {
  Sacnning,
  Connected,
}

class WifiState extends Equatable {

  final WifiCommstate CurrentState;
  final String ssid;
  final String ipAddress;
  final String identifier;

  WifiState ({required this.CurrentState, required this.ssid, required this.ipAddress, required this.identifier});

  WifiState.initial ()
      : CurrentState = WifiCommstate.Sacnning,
        ssid = "",
        ipAddress = "",
        identifier = "";

  WifiState copyWith({
    String? ssid,
    String? ipAddress,
    String? identifier,
    WifiCommstate? CurrentState
  }) {
    return WifiState(
      ssid: ssid ?? this.ssid,
      CurrentState: CurrentState ?? this.CurrentState,
      ipAddress: ipAddress ?? this.ipAddress,
      identifier: identifier ?? this.identifier,
    );
  }
  @override
  List<Object> get props => [CurrentState, ssid, ipAddress, identifier];

}
/*
class WifiScanningState extends WifiState {
  WifiScanningState();
  @override
  List<Object> get props => [];
}

class WifiConnectedState extends WifiState {
  final String ssid;
  final String ipAddress;
  final String identifier;
  WifiConnectedState({@required this.ssid, @required this.ipAddress, this.identifier});

  @override
  List<Object> get props => [ssid];

}
*/
