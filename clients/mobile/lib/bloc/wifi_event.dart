
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';


abstract class WifiEvent extends Equatable {}

class WifiScanningEvent extends WifiEvent {
  final String ssid;
  WifiScanningEvent({required this.ssid});
  @override
  List<Object> get props => [ssid];
}

class WifiConnectedEvent extends WifiEvent {
  final String ssid;
  final String ipaddress;
  WifiConnectedEvent({required this.ssid, required this.ipaddress});
  @override
  List<Object> get props => [];
}

class WifiUnlockDoorEvent extends WifiEvent {
  final String doorName;

  WifiUnlockDoorEvent({required this.doorName});

  @override
  List<Object> get props => [doorName];
}

class WifiDisconnectedEvent extends WifiEvent {
  @override
  List<Object> get props => [];
}
