import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:hackcess/bloc/mqtt_event.dart';
import 'package:hackcess/bloc/mqtt_state.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MqttBloc extends Bloc<MqttEvent, MqttState> {
//  String ipAddress;
  int Port = 1883;
  String clientIdentifier = 'hacklandmobile';
  late mqtt.MqttClient client;
  mqtt.MqttClientConnectionStatus connectionState = MqttClientConnectionStatus();

  MqttBloc () : super(MqttState ()) {
    on<MqttWifiAvailableEvent> ((event, emit) async {
      emit (MqttConnectingState(destAddress: event.ipAddress));
      debugPrint('Start Mqtt connect');
      var connectState = await _connect(event.ipAddress);
      debugPrint('Start Mqtt connect return');
      if (connectState == mqtt.MqttConnectionState.connected) {
        debugPrint('Start Mqtt connect returned connected');
        _subscribe ();
        emit (MqttChangeOfState (connectionState: connectState));
      }

    });

    on<MqttUnlockDoorEvent> ((event, emit) async {
      emit (MqttUnlocDoorState(door: event.door, duration: event.duration));
      await Future.delayed(Duration(seconds: event.duration));
      emit (MqttChangeOfState(connectionState: MqttConnectionState.connected));
    });

    on<MqttDataReceivedEvent> ((event, emit) async {
      emit(MqttDateReceivedState(event.payload));
    });

    on<MqttConnectStateEvent> ((event, emit) async {
      emit(MqttChangeOfState (connectionState: event.mqttState));
    });
  }

  Future<mqtt.MqttConnectionState> _connect(String ipAddress) async {
    client = MqttServerClient(ipAddress, '');
    client.port = Port;
    client.logging(on: false);

    /// If you intend to use a keep alive value in your connect message that is not the default(60s)
    /// you must set it here
    client.keepAlivePeriod = 30;

    client.onDisconnected = onDisconnect;
    client.onConnected = onConnected;
    client.onAutoReconnect = onAutoReconnect;

    client.autoReconnect = true;
    /// Create a connection message to use or use the default one. The default one sets the
    /// client identifier, any supplied username/password, the default keepalive interval(60s)
    /// and clean session, an example of a specific one below.
    final mqtt.MqttConnectMessage connMess = mqtt.MqttConnectMessage()
        .withClientIdentifier(clientIdentifier)
    // Must agree with the keep alive set above or not set
        .startClean() // Non persistent session for testing
        .keepAliveFor(30)
    // If you set this you must set a will message
        .withWillTopic('test/test')
        .withWillMessage('this is my last dyiing gasp')
        .withWillQos(mqtt.MqttQos.atMostOnce);
    print('MQTT client connecting....');
    client.connectionMessage = connMess;

    try {
      await client.connect("peter", "gargoyle");
    } catch (e) {
      print(e);
      //  _disconnect();
    }


    return client.connectionStatus!.state;
//    if (client.connectionState == mqtt.MqttConnectionState.connected) {
  }

  void onAutoReconnect() {
    add(MqttConnectStateEvent(mqttState: client.connectionStatus!.state));
  }

  void onDisconnect() {
    add(MqttConnectStateEvent(mqttState: mqtt.MqttConnectionState.disconnected));
  }

  void onConnected() {
     add(MqttConnectStateEvent(mqttState: mqtt.MqttConnectionState.connected));
     debugPrint('onConnected....${client.connectionStatus!.state.toString()}');
  }

  void _subscribe () {
    debugPrint('Mqtt _subscribe');
    client.subscribe('/device/#', mqtt.MqttQos.atLeastOnce);
    client.updates!.listen((List<MqttReceivedMessage<MqttMessage?>>? messages) {
      messages!.forEach((message) {
        final recMess = message.payload! as MqttPublishMessage;
        if (recMess.variableHeader!.topicName.toLowerCase().contains('unlock')) {
          final pt = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);
          debugPrint('Mqtt paylod rx:$pt');
          int Duration = int.parse(pt);
          add(MqttUnlockDoorEvent(duration: Duration, door: 'hl1door'));
        }
      });
    });
  }

}