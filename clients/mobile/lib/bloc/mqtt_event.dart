

import 'package:equatable/equatable.dart';
import 'package:mqtt_client/mqtt_client.dart';

abstract class MqttEvent extends Equatable {}


class MqttWifiAvailableEvent extends MqttEvent {
  final String ipAddress;
  MqttWifiAvailableEvent({required this.ipAddress});

  @override
  List<Object> get props => [ipAddress];

}

class MqttConnectStateEvent extends MqttEvent {
  final MqttConnectionState mqttState;
  MqttConnectStateEvent({required this.mqttState});

  @override
  List<Object> get props => [mqttState];

}

class MqttDataReceivedEvent extends MqttEvent {
  final String payload;
  MqttDataReceivedEvent({required this.payload});

  @override
  List<Object> get props => [payload];

}

class MqttUnlockDoorEvent extends MqttEvent {
  final String door;
  final int duration;

  MqttUnlockDoorEvent({required this.door, required this.duration});

  @override
  List<Object> get props => [door, duration];

}
