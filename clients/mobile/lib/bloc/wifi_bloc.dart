import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:bloc/bloc.dart';
import 'package:hackcess/api/hackcess-api.dart';
import 'package:hackcess/bloc/mqtt_bloc.dart';
import 'package:hackcess/bloc/wifi_event.dart';
import 'package:hackcess/bloc/wifi_state.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:platform_device_id/platform_device_id.dart';

import 'mqtt_event.dart';


class DeviceIdentifier {
  final String deviceId;

  DeviceIdentifier ({required this.deviceId});
}

class WifiBloc extends Bloc<WifiEvent, WifiState> {

  @override
//  WifiState get initialState => WifiScanningState();
  ConnectivityResult currentWifiState = ConnectivityResult.none;
  final _connectivity = Connectivity();
  final networkInfo = NetworkInfo();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  final HackcessApi api;
  final MqttBloc mqttBloc;
  String wifiSsid = "";
  String localIpAddress = "";
  DeviceIdentifier? identifier;

  WifiBloc(this.api, this.mqttBloc)
      : super(WifiState.initial())
  {
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(onConnectionChanged);
    _getDeviceIdentifier().then((result) {
      identifier = result;
    });

    on<WifiUnlockDoorEvent> ((event, emit) async {
      var result = await api.openDoor(identifier!.deviceId, event.doorName);
      if (result == 403) {

      } else {
        emit ( state.copyWith(ssid: wifiSsid,
            ipAddress: localIpAddress,
            identifier: identifier!.deviceId));
      }

    });

    on<WifiConnectedEvent>((event, emit) async {
      emit(state.copyWith(ssid: event.ssid, ipAddress: event.ipaddress, identifier: identifier!.deviceId, CurrentState: WifiCommstate.Connected));
    });

    on<WifiScanningEvent>((event, emit) async {
      emit(WifiState.initial());
    });
  }

  Future<DeviceIdentifier> _getDeviceIdentifier() async {
    String? id = '';
    try {
       id  = await PlatformDeviceId.getDeviceId;
    } on PlatformException catch (e) {
    }

    return DeviceIdentifier (
      deviceId: id!
    );
  }

  void onConnectionChanged(ConnectivityResult connectivityState) async {
      debugPrint('onConnectionChanged $connectivityState');
      if (connectivityState == ConnectivityResult.wifi && currentWifiState != ConnectivityResult.wifi) {
        currentWifiState = connectivityState;
        debugPrint('WifiScanningState check connectivity it is WIFI');
        wifiSsid = await _getWifiConnectSsid();
        localIpAddress = await _getWifiConnectIpAddress();
        mqttBloc.add(MqttWifiAvailableEvent(ipAddress: api.hostIpAddress));
        add(WifiConnectedEvent(ssid: wifiSsid, ipaddress: localIpAddress));
      }
      currentWifiState = connectivityState;
  }

  Future<ConnectivityResult?> initConnectivity() async {
    ConnectivityResult? result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    print("Get WFIFI connection status");
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    debugPrint("Get WFIFI connection status result");
    return result;
  }

  Future<String> _getWifiConnectIpAddress() async {
    String wifiIP = "0.0.0.0";
    try {
      String? wf = await networkInfo.getWifiIP();
      if (wf != null) {
        wifiIP = wf;
      }
    } on PlatformException catch (e) {
      print(e.toString());
      wifiIP = "Failed to get Wifi IP";
    }
    return wifiIP;
  }

  Future<String> _getWifiConnectSsid() async {
      String wifiName = 'unknown';
      try {
        if (Platform.isIOS) {
/*
          LocationAuthorizationStatus status = await _connectivity.getLocationServiceAuthorization();
          if (status == LocationAuthorizationStatus.notDetermined) {
            status = await _connectivity.requestLocationServiceAuthorization();
          }
          if (status == LocationAuthorizationStatus.authorizedAlways || status == LocationAuthorizationStatus.authorizedWhenInUse) {
            wifiName = await networkInfo.getWifiName();
          } else {
            wifiName = await _connectivity.getWifiName();
          }
*/
        } else {
          String? w = await networkInfo.getWifiName();
          if (w != null)
            wifiName = w;
          debugPrint("got wifi name ${wifiName}");
        }
      } on PlatformException catch (e) {
        print(e.toString());
        wifiName = "Failed to get Wifi Name";
      }
      return wifiName;
  }
}