import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mqtt_client/mqtt_client.dart';

import 'bloc/mqtt_bloc.dart';
import 'bloc/mqtt_state.dart';

class MqttStatus extends StatefulWidget {
  @override
  _MqttStatusState createState() => _MqttStatusState();
}

class _MqttStatusState extends State<MqttStatus> with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation animation;

  @override
  void initState() {
    animationController = AnimationController(vsync: this, duration: Duration(seconds: 2));
    animationController.repeat(reverse: true);
    animation = Tween(begin: 2.0, end: 15.0).animate(animationController)..addListener((){
      setState(() {

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: BlocBuilder<MqttBloc, MqttState>(
            builder: (context, state) {
              if (state is MqttUnlocDoorState) {
                return _showMqttStatus (state.door, Colors.green);
              }
              if (state is MqttChangeOfState) {
                Color c = Colors.pink;
                String text = 'Disconnected';
                if (state.connectionState == MqttConnectionState.connected) {
                  c = Colors.orange;
                  text = 'Connected';
                } else if (state.connectionState == MqttConnectionState.connecting) {
                  c = Colors.orange;
                  text = 'Connecting';
                }
                return _showMqttStatus (text, c);
              }
              if (state is MqttDateReceivedState) {
                return Center (
                  child: Text(state.payload, style: TextStyle(fontSize: 32)),
                );

              }
              return _showMqttStatus ('disconnected', Colors.pink);

            }
        )
    );
  }

  Widget _showMqttStatus (String text, Color c) {
    return Container(
      height: 100,
      margin: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: c,
        boxShadow: [BoxShadow(
          color: Colors.orange,
          blurRadius: animation.value,
          spreadRadius: animation.value,
        )],
      ),
      child: Center (

        child: Text(text, style: TextStyle(fontSize: 32)),
      ),
    );
  }
}
