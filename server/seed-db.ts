import Device from './src/models/Device';
import User from './src/models/User';
import Token from './src/models/Token';
import DeviceEvent from './src/models/Event';

(async function () {
    const simon = await User.addNew({
        name: 'Simon Werner',
        email: 'simonwerner@gmail.com',
        admin: true,
        slackUserId: 'U93KS1E8N',
    });
    const cam = await User.addNew({
        name: 'Cameron Stewart',
        email: 'cstewart000@gmail.com',
        userId: '67e45b1e-843e-4c57-9156-e4eae1157f84',
        enabled: true,
        admin: true,
    });
    const fang = await User.addNew({
        name: 'Fang ',
        email: '',
        userId: 'c7424ec8-645c-478a-9bb9-288c958a1228',
        enabled: true,
        admin: false,
    });

    // Simon's rfid
    let t = await Token.addNew('DD6F9E29');
    t.lastSeen = '2020-05-08T06:21:24.073Z';
    await t.save();
    await t.assignUser(simon.userId);

    t = await Token.addNew('B7B0D5A4');
    t.lastSeen = '2020-05-08T06:21:24.073Z';
    await t.save();
    await t.assignUser(simon.userId);

    // Other rfid cards
    t = await Token.addNew('CC57C8A5');

    t = await Token.addNew('044120A2935680');
    t.lastSeen = '2020-05-08T05:08:05.229Z';
    await t.save();
    await t.assignUser(cam.userId);

    t = await Token.addNew('30A0CD5D');
    t.lastSeen = '2020-05-07T23:43:51.556Z';
    await t.save();

    t = await Token.addNew('55D125D9');
    t.lastSeen = '2020-05-07T23:42:07.549Z';
    await t.save();

    t = await Token.addNew('046DB052FC6381');
    t.lastSeen = '2020-05-08T05:00:54.120Z';
    await t.save();
    await t.assignUser(fang.userId);

    t = await Token.addNew('048B3592065380');
    t.lastSeen = '2020-05-08T05:08:09.224Z';
    await t.save();
    await t.assignUser(cam.userId);

    // HL++
    // /device/84F3EB770B19/register rfid-reader
    const hl2_door_reader = await Device.registerDevice('84F3EB770B19', 'rfid-reader');
    await hl2_door_reader.setLocation('Hackland++');
    await hl2_door_reader.enable();

    // /device/600194202C6E/register lock
    const hl2_door_lock = await Device.registerDevice('600194202C6E', 'lock');
    await hl2_door_lock.setLocation('Hackland++');
    await hl2_door_lock.enable();

    await DeviceEvent.create({
        type: 'card',
        deviceUid: '84F3EB770B19',
        actions: [
            {
                actionType: 'unlock',
                deviceUid: '600194202C6E',
                message: '3',
            },
        ],
    });

    // HL
    // /device/840D8E8FF4B8/register rfid-reader
    const hl1_door_reader = await Device.registerDevice('840D8E8FF4B8', 'rfid-reader');
    await hl1_door_reader.setLocation('Hackland1');
    await hl1_door_reader.enable();

    const hl1_door_lock = await Device.registerDevice('84F3EB770609', 'lock');
    await hl1_door_lock.setLocation('Hackland1');
    await hl1_door_lock.enable();

    await DeviceEvent.create({
        type: 'card',
        deviceUid: '840D8E8FF4B8',
        actions: [
            {
                type: 'unlock',
                deviceUid: '84F3EB770609',
                message: '3',
            },
        ],
    });

    console.log('done');

    // Other devices seen are:
    //   "600194202C80"
    //   "5ccf7f0a19be"
    //   "ECFABCB40DD8"

    // /device/mobile-Hackland1/register mobile
    const hl1_door_mobile = await Device.registerDevice('mobile-hl1door', 'mobile');
    await hl1_door_mobile.setLocation('Hackland1');
    await hl1_door_mobile.enable();

    // /device/mobile-Hackland++/register mobile
    const hl2_door_mobile = await Device.registerDevice('mobile-hl2door', 'mobile');
    await hl2_door_mobile.setLocation('Hackland++');
    await hl2_door_mobile.enable();
})();
