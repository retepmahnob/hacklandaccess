import mongoose from '../mongo';
import User, { UserModel } from './User';
import Token, { TokenModel } from './Token';
import _ from 'lodash';
import common from '../common';

export interface IAccessLog extends mongoose.Document {
    createdAt: Date;
    token: String; // The token id
    userId: String; // The user this token is associated with.  If undefined, then it's not active
    location: String; // The location accessed
    authorised: Boolean; // Was the entry authorised?
}

export const accessLogSchema = new mongoose.Schema({
    createdAt: { type: Date, expires: '30d', default: Date.now },
    token: { type: String, required: true },
    userId: { type: String, required: false },
    location: { type: String, required: true },
    authorised: { type: Boolean, required: true },
});

export class AccessLogModel extends mongoose.Model {
    //
    // Static functions that work on the whole Token Data model
    //
    public static async getAll(limit: number): Promise<AccessLogModel[]> {
        const logEntries = await this.find()
            .sort({ createdAt: 'descending' })
            .limit(limit * 1);

        // Need to lookup each user too
        for (const log of logEntries) {
            log.date = common.localDate(log.createdAt);
            if (log.userId) {
                log.user = await User.findByUserId(log.userId);
            } else {
                log.user = await Token.get(log.token).then((token: TokenModel) =>
                    token ? token.getUser() : undefined
                );
            }
        }
        return logEntries;
    }

    public static async log(user: UserModel, token: string, location: string, authorised: boolean): Promise<void> {
        await this.create({
            token,
            userId: user ? user.userId : undefined,
            location,
            authorised,
        });
    }
}

accessLogSchema.loadClass(AccessLogModel);
const AccessLog = <any>mongoose.model<IAccessLog>('AccessLog', accessLogSchema);
export default AccessLog;
