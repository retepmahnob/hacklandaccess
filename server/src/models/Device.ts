/**
 * Device model. Describes the devices and their events.
 *
 * Events can be triggered by devices or the system.  When an event is triggered, there may Actions which occur
 * based on the event.  Here we map the events to the actions. For example it can be read like
 * "On DeviceEvent.type do all the actions in DeviceEvent.actions".
 */

import mongoose from '../mongo';
import common from '../common';
import Action from './Action';
import fileUpload from 'express-fileupload';

const MAX_LOGS = 100;

/*
 * The list of the possible actions and events for each device type.
 */
const deviceEventActions: any = {
    // Creates 'card' events which translate to 'unlock' events.
    'rfid-reader': {
        actions: ['unlock'],
        event: 'card',
    },

    // Creates 'card' events which translate to 'unlock' events.
    mobile: {
        actions: ['unlock'],
        event: 'card',
    },
};

function assertDeviceType(str: string): boolean {
    switch (str) {
        case 'lock':
            return true;
        case 'rfid-reader':
            return true;
        case 'mobile':
            return true;
        default:
            throw new Error(`Invalid DeviceType: ${str}`);
    }
}

export interface IDevice extends mongoose.Document {
    type: string; // What kind of device this is
    uid: string; // The UID (typically the mac address) of the device
    enabled: boolean; // If false, then this device will be ingored
    lastSeen: string; // When was this device last seen
    location: string; // The location of this device
    state: string; // The state of the device
    stateChangeTime: string; // When the state last changed, or was first set
    slackChannelId: string; // The members in this slack channel have access to this
    event: {
        type: string; // The type of event.  E.g. 'card'
        actions: Action[]; // The list of actions triggered by this event
    };
    // logs: string;

    // Values not saved to the DB
    createsEvents: boolean;
    emitsActions: boolean;
    possibleActions: string[];
    possibleEvents: string[];
}

export const deviceSchema = new mongoose.Schema({
    type: { type: String, required: true },
    uid: { type: String, required: true },
    enabled: { type: Boolean, default: false },
    lastSeen: { type: String, required: true },
    location: { type: String, required: false },
    state: { type: String, required: false },
    stateChangeTime: { type: String, required: false },
    slackChannelId: { type: String, required: false },
    event: {
        type: { type: String, required: false },
        actions: { type: [], required: false },
    },
    // logs: { type: String, default: '[]' },
});

// This is executed when we pull a document out of the DB.  We need to modify the "actions"
// array to make it a list of `new Action()` objects.
deviceSchema.post('findOne', function (device: any) {
    if (!device) return;

    // List of possible actions to handle
    if (deviceEventActions[device.type]) {
        device.possibleActions = deviceEventActions[device.type].actions;
        device.emitsActions = device.possibleActions.length > 0;
    } else {
        device.emitsActions = false;
    }

    // Neaten up the actions
    if (device.event) {
        device.event.actions = device.event.actions.map(Action.fromObj);
    }
});

export class DeviceModel extends mongoose.Model {
    public async setLocation(location: string): Promise<void> {
        this.location = location;
        await this.save();
    }

    // Enable a device for a given function
    public async enable(): Promise<void> {
        this.enabled = true;
        await this.save();
    }

    // When a device is touched, it means it's been seen on the network.
    // TODO: Use http://www.steves-internet-guide.com/checking-active-mqtt-client-connections
    //       to create a last will and testimant.
    public async touch(): Promise<void> {
        this.lastSeen = common.time();
        await this.save();
    }

    public async appendAction(action: Action): Promise<DeviceModel> {
        const eventType = deviceEventActions[this.type].event;
        if (eventType && !(this.event && this.event.type)) {
            this.event = {
                type: eventType,
                actions: [],
            };
        }
        this.event.actions.push(action);

        await this.save();
        return this;
    }

    // Remove the last action from the list
    public async popAction(): Promise<DeviceModel> {
        if (this.event && this.event.actions && this.event.actions.length > 0) {
            this.event.actions.pop();
        }

        await this.save();
        return this;
    }

    public async setState(newState: string): Promise<DeviceModel> {
        if (newState == this.state) {
            return this;
        }
        this.stateChangeTime = common.time();
        this.state = newState;
        return await this.save();
    }

    //
    // Static functions that work on the whole User Data model - these could be moved to a UserManager class
    //

    public static async createUnregisteredDevice(uid: string, type: string): Promise<DeviceModel> {
        assertDeviceType(type);
        const device = await this.create({
            uid,
            type,
            lastSeen: common.time(),
            enabled: false,
        });
        return device;
    }

    public static async findByUid(uid: string): Promise<DeviceModel> {
        return await this.findOne({ uid });
    }

    public static async findOthers(uid: string): Promise<DeviceModel[]> {
        return await this.find({ uid: { $ne: uid } });
    }

    public static async findEnabledByUid(uid: string): Promise<DeviceModel> {
        return await this.findOne({ uid, enabled: true });
    }

    public static async findByUidAndRemove(uid: string): Promise<void> {
        return await this.findOne({ uid }).remove();
    }

    public static async getAll(): Promise<DeviceModel[]> {
        return (await this.find({})).map((device: any) => {
            device.lastSeenLocal = common.localDate(device.lastSeen);
            return device;
        });
    }

    public static async getAllOfType(type: string): Promise<DeviceModel[]> {
        assertDeviceType(type);
        return (await this.find({ type })).map((device: any) => {
            return device;
        });
    }

    public static async getTypeAndLocation(type: string, location: string): Promise<DeviceModel[]> {
        assertDeviceType(type);
        return (await this.find({ type, location })).map((device: any) => {
            return device;
        });
    }

    public static async appendLog(uid: string, log: string): Promise<void> {
        const device: DeviceModel = await this.findByUid(uid);
        if (!device) return;

        // const logEntry = `${common.time()}: ${log}`;
        // const logs = device.logs instanceof Array ? device.logs : JSON.parse(device.logs);
        // logs.unshift(logEntry);
        // if (logs.length > MAX_LOGS) {
        //     logs.pop();
        // }
        // device.logs = JSON.stringify(logs);

        await device.save().catch(console.error);
    }

    public static async registerDevice(uid: string, type: string): Promise<DeviceModel> {
        assertDeviceType(type);
        const device = await this.findOne({ uid });

        if (!device) {
            console.log(`DeviceManager: Registering new device: id: ${uid} as ${type}`);
            return await this.createUnregisteredDevice(uid, type);
        }

        device.type = type;
        await device.touch();

        if (device.enabled) {
            console.log(`DeviceManager: Device already registered and enabled`);
        } else {
            console.log(`DeviceManager: Device already registered, but not enabled: id:${uid} as ${type}`);
        }

        return device;
    }

    public static async upgradeFirmware(deviceUid: string, firmware: fileUpload.UploadedFile): Promise<Action> {
        const filename = `${deviceUid}.bin`;
        const filepath = `/tmp/${filename}`;
        await firmware.mv(filepath);

        return new Action({
            type: 'upgrade',
            message: `firmware/${filename}`,
            deviceUid,
        });
    }
}

deviceSchema.loadClass(DeviceModel);
const Device = <any>mongoose.model<IDevice>('Device', deviceSchema);
export default Device;
