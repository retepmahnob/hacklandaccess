/**
 * An action occurs from an event and targets another device.
 */
export default class Action {
    readonly type: string; // The type of action to perform when the action occurs. e.g. 'unlock'
    readonly message: string; // The parameters of the action
    readonly deviceUid: string; // The device this action applies to

    constructor(obj: any) {
        this.type = obj.type;
        this.message = obj.message;
        this.deviceUid = obj.deviceUid;
    }

    toObj(): Object {
        return {
            type: this.type,
            message: this.message,
            deviceUid: this.deviceUid,
        };
    }

    isAuth(): Boolean {
        return this.type === 'auth';
    }

    isSuccess(): Boolean {
        return this.type === 'auth' && this.message === 'success';
    }

    //
    // Static functions
    //

    static fromObj(obj: any): Action {
        return new Action(obj);
    }

    public static success(deviceUid: string) {
        return new Action({
            type: 'auth',
            message: 'success',
            deviceUid: deviceUid,
        });
    }

    public static denied(deviceUid: string) {
        return new Action({
            type: 'auth',
            message: 'failure',
            deviceUid: deviceUid,
        });
    }

    public static buzzerOn(deviceUid: string) {
        return new Action({
            type: 'buzzer',
            message: 'on',
            deviceUid: deviceUid,
        });
    }

    public static buzzerOff(deviceUid: string) {
        return new Action({
            type: 'buzzer',
            message: 'off',
            deviceUid: deviceUid,
        });
    }
}
