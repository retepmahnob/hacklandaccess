/**
 * These are the routes where the admin administers user profiles.
 */

import mongoose from '../mongo';
import { v4 as uuidv4 } from 'uuid';
import common from '../common';
import Config from '../configuration';

export interface IUser extends mongoose.Document {
    name: string; // Name of the user
    userId: string; // Unique ID of the user
    admin: boolean; // Is the user an administrator
    email: string; // The user's email address
    enabled: boolean; // Is the user enabled?
    slackUserId: string; // The user's slack Id
    accessChannels: [string]; // Which slack channels the user has access to
}

export const userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    userId: { type: String, required: false },
    admin: { type: Boolean, default: false },
    email: { type: String, required: false },
    enabled: { type: Boolean, required: false },
    slackUserId: { type: String, required: false },
    accessChannels: { type: [String], required: false },
});

export class UserModel extends mongoose.Model {
    // public isAuthenticated(channelId: string): boolean {
    //     return this.enabled && (await this.accessChannels.includes[channelId]);
    // }
    public isAuthenticated(): boolean {
        return this.enabled;
    }

    public async syncWithSlack(accessChannels: Array<string>): Promise<void> {
        this.accessChannels = accessChannels;
        this.enabled = this.accessChannels.includes(Config.slackSupportersChannelId);
        return await this.save();
    }

    public async setSlackUserId(slackUserId: string): Promise<void> {
        this.slackUserId = slackUserId;
        return await this.save();
    }

    public async touch(): Promise<UserModel> {
        this.lastSeen = common.time();
        return await this.save();
    }

    public memberStatus(): string {
        if (!this.isAuthenticated()) {
            return 'Not a paying member - no access';
        } else if (this.admin) {
            return 'System admin';
        } else {
            return 'Paying member';
        }
    }

    //
    // Static functions that work on the whole User Data model - these could be moved to a UserManager class
    //
    public static async addNew(userDetails: any): Promise<UserModel> {
        userDetails.userId = uuidv4();
        userDetails.enabled = true;
        userDetails.accessChannels = ['supporters'];
        return await this.create(userDetails);
    }

    public static async addFromSlack(
        slackUserId: string,
        slackUserName: string,
        accessChannels: string
    ): Promise<UserModel> {
        const user: UserModel = await this.addNew({ name: slackUserName });
        user.slackUserId = slackUserId;
        user.accessChannels = accessChannels;
        user.enabled = accessChannels.includes(Config.slackSupportersChannelId);

        await user.save();
        return user;
    }

    public static async findByUserId(userId: string): Promise<UserModel> {
        return await this.findOne({ userId });
    }

    public static async findByUserIdAndRemove(userId: string): Promise<void> {
        return await this.findOne({ userId }).remove();
    }

    public static async findByName(name: string): Promise<UserModel> {
        return await this.findOne({ name });
    }

    public static async getAll(): Promise<UserModel[]> {
        return await this.find({});
    }

    public static async findUserByEmail(email: string): Promise<UserModel> {
        return await this.findOne({ email });
    }

    public static async updateByUserId(userId: string, params: any): Promise<UserModel> {
        return this.findOneAndUpdate({ userId }, params);
    }

    public static async findBySlackId(slackUserId: string): Promise<UserModel | undefined> {
        return this.findOne({ slackUserId });
    }
}

userSchema.loadClass(UserModel);
const User = <any>mongoose.model<IUser>('User', userSchema);
export default User;
