/**
 * This model connects to Slack on start and downloads all the channels and
 * all the members within the channels.  It will store these in mongo db.
 * It will also requery Slack every `slackChannelUpdateFrequencySeconds`.
 *
 * It may be able to improve this.  Slack has released a new API and is
 * deprecating the old simpler one.  This means for each user we need to
 * test if they are in a channel, previously you could just see which
 * users are in a channel.
 */

import * as slack from '@slack/web-api';
import Config from '../configuration';
import mongoose from '../mongo';
import User from './User';

//
// Types
//

interface ISlackUser extends mongoose.Document {
    readonly id: string; // The slack user ID
    readonly name: string; // The name of the user
    readonly realName: string; // The real_name of the user
    readonly channelIds: Array<string>;
}
export type SlackUserType = Readonly<ISlackUser>;

interface ISlackChannel extends mongoose.Document {
    readonly id: string; // The slack channel ID
    readonly name: string; // The name of the channel
    readonly purpose: string; // The purpose of the channel
}
export type SlackChannelType = Readonly<ISlackChannel>;

//
// Database Models
//

interface ISlackData extends mongoose.Document {
    members: Array<SlackUserType>;
    channels: Array<SlackChannelType>;
}

const slackDataSchema = new mongoose.Schema({
    members: { type: [Object], required: true },
    channels: { type: [Object], required: true },
});

class SlackDataModel extends mongoose.Model {
    public static async init(): Promise<SlackDataModel> {
        return await this.findOne({});
    }

    private async updateHacklandUser(member: SlackUserType): Promise<void> {
        const user = await User.findBySlackId(member.id);
        if (!user) {
            await User.addFromSlack(member.id, member.realName, member.channelIds);
        } else {
            await user.syncWithSlack(member.channelIds);
        }
    }

    /**
     * Update all of the Slack details.  Members and hidden channels.
     */
    public async fetchFromSlack(webSlack: slack.WebClient) {
        let tmpMembers = await this.fetchAllSlackUsers(webSlack);
        let tmpChannels = {};

        for (const member of tmpMembers) {
            console.log(member.id, member.name);
            await this.fetchAllChannelsForUser(webSlack, member, tmpChannels);
        }

        // Purge members without a channel
        this.members = tmpMembers.filter((member: SlackUserType) => member.channelIds.length !== 0);
        this.channels = Object.values(tmpChannels); // Convert to an array
        console.log(`Retreived ${this.members.length} members in ${this.channels.length} channel`);

        // Update the Hackland members users
        for (let member of this.members) {
            await this.updateHacklandUser(member);
        }

        await this.save();
    }

    private async fetchAllSlackUsers(webSlack: slack.WebClient) {
        const result: any = await webSlack.users.list({ include_locale: false });

        return result.members
            .filter((member: any) => !member.deleted && !member.is_bot)
            .map((member: any) => ({
                id: member.id,
                name: member.name,
                realName: member.real_name,
                channelIds: [],
            }));
    }

    /**
     * For each member we need to fetch the channel they belong to.
     */
    private async fetchAllChannelsForUser(webSlack: slack.WebClient, member: SlackUserType, channels: any) {
        const result: any = await webSlack.users.conversations({
            exclude_archived: true,
            types: 'private_channel',
            user: member.id,
        });
        member.channelIds.length = 0;
        result.channels
            .filter((channel: any) => channel.is_mpim !== true && channel.is_im !== true)
            .forEach((channel: any) => {
                member.channelIds.push(channel.id);
                channels[channel.id] = {
                    id: channel.id,
                    name: channel.name,
                    purpose: channel.purpose.value,
                };
            });
    }

    // use "https://api.slack.com/methods/users.list" to get the list of all Members
    // 	    - users:read
    // use "https://api.slack.com/methods/users.conversations"
    //      - channels:read  groups:read  im:read  mpim:read
}

slackDataSchema.loadClass(SlackDataModel);
const SlackData = <any>mongoose.model<ISlackData>('SlackData', slackDataSchema);

export default class SlackModel {
    public webSlack: slack.WebClient;
    private slackData: SlackDataModel;

    constructor() {
        this.slackData = SlackData;
        this.webSlack = new slack.WebClient(Config.slackToken);
    }

    public async connect() {
        await this.webSlack.auth.test({ token: Config.slackToken });
        this.slackData = await SlackData.init();
        // await this.slackData.fetchFromSlack(this.webSlack);
        setInterval(
            async () => await this.slackData.fetchFromSlack(this.webSlack),
            Config.slackChannelUpdateFrequencySeconds * 1000
        );
    }

    public async getSlackChannels(): Promise<Array<SlackChannelType>> {
        return this.slackData.channels;
    }

    public getChannelById(channelId: string): Promise<SlackChannelType> {
        return this.slackData.channels.find((channel: SlackChannelType) => channel.id === channelId);
    }

    public async getMembersInChannel(channelId: string): Promise<Array<SlackUserType>> {
        return this.slackData.members
            .filter((member: SlackUserType) => member.channelIds.includes(channelId))
            .sort((a: SlackUserType, b: SlackUserType) => {
                if (a.realName == b.realName) return 0;
                return a.realName < b.realName ? -1 : 1;
            });
    }

    public async getMemberById(slackUserId: string): Promise<SlackUserType> {
        return this.slackData.members.find((member: SlackUserType) => member.id === slackUserId);
    }
}
