import mongoose from '../mongo';
import common from '../common';
import User, { UserModel } from './User';

export interface IToken extends mongoose.Document {
    _id: String; // The UUID
    token: String; // The token id
    lastSeen: String; // When was this token last seen
    userId: String; // The user this token is associated with.  If undefined, then it's not active
}

export const tokenSchema = new mongoose.Schema({
    token: { type: String, required: true },
    lastSeen: { type: String, required: true },
    userId: { type: String, required: false },
});

export class TokenModel extends mongoose.Model {
    public async getUser(): Promise<UserModel> {
        return User.findByUserId(this.userId);
    }

    // Assign a userId to a token
    public async assignUser(userId: string): Promise<TokenModel> {
        this.userId = userId;
        await this.save();
        return this;
    }

    // Remove the user from the token
    public async clearUser(): Promise<TokenModel> {
        this.userId = undefined;
        await this.save();
        return this;
    }

    public async touch(): Promise<TokenModel> {
        this.lastSeen = common.time();
        return await this.save();
    }

    //
    // Static functions that work on the whole Token Data model
    //
    public static async addNew(token: String): Promise<TokenModel> {
        if (await this.findOne({ token })) {
            throw new Error('Token already exists');
        }
        return await this.create({
            token,
            lastSeen: common.time(),
        });
    }

    public static async get(token: String): Promise<TokenModel | undefined> {
        let t = await this.findOne({ token });
        if (!t) {
            this.addNew(token);
        }
        return t;
    }

    public static async getRecent(): Promise<TokenModel[]> {
        const fiveMinutesAgo = new Date().getTime() - 5 * 60 * 1000;
        const tokenList: TokenModel[] = await this.find({}).sort({ lastSeen: 'descending' });

        for (const token of tokenList) {
            token.lastSeenLocal = common.localDate(token.lastSeen);
        }

        return tokenList
            .filter((token: TokenModel) => !token.userId)
            .filter((token) => fiveMinutesAgo < new Date(token.lastSeen).getTime())
            .map((token: TokenModel) => {
                token.lastSeenLocal = common.localDate(token.lastSeen);
                return token;
            });
    }

    public static async getAll(limit: number): Promise<TokenModel[]> {
        const tokenList: TokenModel[] = await this.find({})
            .sort({ lastSeen: 'descending' })
            .limit(limit * 1);

        // Need to lookup each user too
        for (const token of tokenList) {
            token.user = await token.getUser();
            token.lastSeenLocal = common.localDate(token.lastSeen);
        }
        return tokenList;
    }

    public static async getForUser(userId: string): Promise<TokenModel[]> {
        const tokenList: TokenModel[] = await this.find({ userId }).sort({ lastSeen: 'descending' });

        for (const token of tokenList) {
            token.lastSeenLocal = common.localDate(token.lastSeen);
        }

        return tokenList.map((token: TokenModel) => {
            token.lastSeenLocal = common.localDate(token.lastSeen);
            return token;
        });
    }
}

tokenSchema.loadClass(TokenModel);
const Token = <any>mongoose.model<IToken>('Token', tokenSchema);
export default Token;
