import HomeController from './controllers/home.controller';
import TokenController from './controllers/token.controller';
import UserController from './controllers/user.controller';
import DeviceController from './controllers/device.controller';
import AccessLogController from './controllers/accessLog.controller';
import SlackController from './controllers/slack.controller';
import AuthController from './controllers/auth.controller';
import SelfController from './controllers/self.controller';
import StateController from './controllers/state.controller';

// import * as cors from 'cors';
import express from 'express';
import * as BodyParser from 'body-parser';
import session from 'express-session';
import * as Sentry from '@sentry/node';
import fileUpload from 'express-fileupload';

import { requestLoggerMiddleware } from './request.logger.middleware';
import SlackModel from './models/Slack';
import PubSub from './pubsub';
import HacklandEvents from './HacklandEvents';
import Config from './configuration';
import mongoose from './mongo';
mongoose.set('useCreateIndex', true); // Avoid deprication warnings

const MongoStore = require('connect-mongo')(session);
Sentry.init({ dsn: Config.sentryDsn, environment: Config.environment });

const port = process.env.PORT || 8088;

const app = express();
//app.use(cors());
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(requestLoggerMiddleware);
app.use(express.static('static'));
app.use(
    session({
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
        secret: 'cats run rampent in hackland',
        saveUninitialized: false, // don't create session until something stored
        resave: false, //don't save session if unmodified
    })
);
app.use(
    fileUpload({
        limits: { fileSize: 50 * 1024 * 1024 },
    })
);
// FIXME: This is a security risk
app.use('/firmware', express.static('/tmp'));

// Authorisation
const authController = new AuthController();
app.use(authController.passport.initialize());
app.use(authController.passport.session());
app.use(authController.renderRequestMiddleware());
app.use(authController.authenticateRouteMiddleware()); // All requests get authenticated

// Our Hackland Controllers
const hacklandEvents = new HacklandEvents();
const homeController = new HomeController();
const tokenController = new TokenController();
const deviceController = new DeviceController(hacklandEvents);
const accessLogController = new AccessLogController();
const slackModel = new SlackModel();
const slackController = new SlackController(slackModel);
const userController = new UserController(slackModel);
const selfController = new SelfController();

// This just runs
new StateController();

app.set('view engine', 'pug');
app.set('views', './src/views');

app.use(authController.Routes);
app.use(homeController.Routes);
app.use(userController.Routes);
app.use(tokenController.Routes);
app.use(deviceController.Routes);
app.use(accessLogController.Routes);
app.use(slackController.Routes);
app.use(selfController.Routes);
app.listen(port, async () => {
    try {
        await PubSub.connect(hacklandEvents);
        await slackModel.connect();
    } catch (err) {
        console.error(err);
    }
});
