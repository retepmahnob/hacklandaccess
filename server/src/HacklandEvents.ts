import Device from './models/Device';
import Token from './models/Token';
import { UserModel } from './models/User';
import Action from './models/Action';
import AccessLog from './models/AccessLog';
import PubSub from './pubsub';
import StateController from './controllers/state.controller';

/**
 * Loads a list of device from `device_list.json` and manages them
 */
export default class HacklandEvents {
    private executeAllActions(actions: Action[]) {
        actions.forEach(async (action) => {
            const targetDevice = await Device.findEnabledByUid(action.deviceUid);
            if (!targetDevice) return;
            await PubSub.emitMQTTMessage(action);
        });
    }

    async doAction(action: Action): Promise<void> {
        await this.executeAllActions([action]);
    }

    private async validateToken(tokenStr: string): Promise<any> {
        // Get the token, no registered token, no access.
        const token = await Token.get(tokenStr);
        if (!token) {
            console.error(`onEvent: Unable to find the token: ${tokenStr}`);
            return { success: false };
        }
        await token.touch();

        // Check the user matches an rfid token
        const user = await token.getUser();
        if (!user) {
            console.error(`onEvent: No user found with token=${token.token}`);
            return { success: false, token };
        }
        await user.touch();

        // Check the user is authenticated
        if (!user.isAuthenticated()) {
            console.error(`onEvent: User not authorised: token=${token}`);
            return { success: false, user, token };
        }
        return { success: true, user, token };
    }

    async onTokenEvent(deviceId: string, tokenStr: string): Promise<Action> {
        // Get here and we have an event that is more complex and needs user authentication

        // Validate the device is active
        const sourceDevice = await Device.findEnabledByUid(deviceId);
        if (!sourceDevice) {
            console.error(`onEvent: Unable to find device with id: ${deviceId}`);
            return Action.denied(deviceId);
        }
        await sourceDevice.touch();

        const { success, user, token } = await this.validateToken(tokenStr);
        if (!success) {
            let tokenStr = 'unknown token';
            if (token && token.token) {
                tokenStr = token.token;
            } else if (token) {
                tokenStr = token;
            }
            await AccessLog.log(user, tokenStr, sourceDevice.location, false);
            return Action.denied(deviceId);
        }

        // The validated event that occurred
        await AccessLog.log(user, token.token, sourceDevice.location, true);
        await this.executeAllActions(sourceDevice.event.actions);

        // Authenticated
        return await Action.success(deviceId);
    }

    async onAuthenticatedUserEvent(deviceId: string, user: UserModel): Promise<Action> {
        // Get here and we have an event that is more complex and needs user authentication

        // Validate the device is active
        const sourceDevice = await Device.findEnabledByUid(deviceId);
        if (!sourceDevice) {
            console.error(`onEvent: Unable to find device with id: ${deviceId}`);
            return Action.denied(deviceId);
        }
        await sourceDevice.touch();

        // The validated event that occurred
        await AccessLog.log(user, deviceId, sourceDevice.location, true);
        await this.executeAllActions(sourceDevice.event.actions);

        // Authenticated
        return await Action.success(deviceId);
    }

    async handleEvent(eventStr: string, deviceId: string, message: any): Promise<any> {
        console.log(`handleEvent: deviceId: ${deviceId}, action: ${eventStr}, message: ${message}`);

        switch (eventStr) {
            case 'register': {
                // Register is a basic event where we just register a device
                const type = message;
                await Device.registerDevice(deviceId, type);
                return null;
            }
            case 'card': {
                // An RFID card reader event has occurred
                const token = message;
                return await this.onTokenEvent(deviceId, token);
            }
            case 'web-user': {
                // A user via the web page
                const user = message;
                return await this.onAuthenticatedUserEvent(deviceId, user);
            }
            case 'log': {
                // A log is coming from the device
                const log = message;
                return await Device.appendLog(deviceId, log);
            }
            case 'state': {
                // The device has changed state
                const newState = message;
                return await StateController.handleStateChange(deviceId, newState);
            }

            case 'upgrade':
            case 'version':
            case 'unlock':
            case 'auth': {
                // Don't care about these cases, this is an action, not an event
                return null;
            }
            default: {
                console.error(`Unhandled event: eventStr: ${eventStr}, deviceId: ${deviceId}, message: ${message}`);
                return null;
            }
        }
    }
}
