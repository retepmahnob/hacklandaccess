import mqtt from 'mqtt';

import Config from './configuration';
import Action from './models/Action';
import HacklandEvents from './HacklandEvents';
import { ServerStatus } from './mqttMessage';

console.log(`Connect to ${Config.mqttConnectString}`);
const mqttClient = mqtt.connect(Config.mqttConnectString);

const PubSub = {
    pub: async (topic: string, message: string) => {
        if (mqttClient) {
            mqttClient.publish(topic, message);
        }
    },

    emitMQTTMessage: async (action: Action) => {
        const mqttTopic = `/device/${action.deviceUid}/${action.type}`;
        const mqttMessage = action.message.toString();
        if (mqttClient) {
            console.log(mqttTopic, mqttMessage);
            mqttClient.publish(mqttTopic, mqttMessage);
        } else {
            console.error('MQTT Client not set');
        }
        return action;
    },

    connect: async (hacklandEvent: HacklandEvents) => {
        mqttClient.on('connect', () => {
            console.log(`Mqtt Connected`);
            if (mqttClient) {
                let s: ServerStatus = { IpAddress: Config.localIpAddress, Status: 'Online' };
                mqttClient.publish('/server', JSON.stringify(s), { retain: true, qos: 0 });
                mqttClient.subscribe('/device/#');
            }
        });

        mqttClient.on('message', async (topic: any, message: any) => {
            const msg = message.toString();
            console.log(`Got mqtt topic: ${topic} message: ${msg}`);
            const topicArr = topic.split('/');
            const name = topicArr[1];

            if (name === 'device' && topicArr.length >= 3) {
                const deviceId = topicArr[2];
                const eventStr = topicArr[3];

                hacklandEvent
                    .handleEvent(eventStr, deviceId, msg)
                    .then((response: any) => {
                        if (response && response.isAuth()) {
                            return PubSub.emitMQTTMessage(response);
                        }
                    })
                    .then(() => {})
                    .catch(console.error);
            }
        });
    },
};
export default PubSub;
