import * as express from 'express';
import Device, { DeviceModel } from '../models/Device';
import Action from '../models/Action';
import HacklandEvents from '../HacklandEvents';
import common from '../common';

export class DeviceController {
    public Routes = express.Router();
    private hacklandEvents: HacklandEvents;

    constructor(hacklandevents: HacklandEvents) {
        this.hacklandEvents = hacklandevents;
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        //
        // WWW access methods
        //

        this.Routes.get('/devices', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('devices', { devices: await Device.getAll() })
        );

        this.Routes.get(
            '/device/:uid/edit',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                resp.render('deviceEdit', { device: await Device.findByUid(req.params.uid) })
        );

        this.Routes.post(
            '/device/:uid/edit',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                req.body.enabled = req.body.enabled == 'on';
                Device.findOneAndUpdate(
                    {
                        uid: req.params.uid,
                    },
                    req.body
                )
                    .then(() => {
                        resp.redirect('/devices');
                    })
                    .catch(common.error(resp));
            }
        );

        this.Routes.post(
            '/device/:uid/upgrade',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                // @ts-ignore
                Device.upgradeFirmware(req.params.uid, req.files.bin)
                    .then((action: Action) => this.hacklandEvents.doAction(action))
                    .then(() => {
                        resp.redirect(`/device/${req.params.uid}/logs`);
                    })
                    .catch(common.error(resp));
            }
        );

        this.Routes.get(
            '/device/:uid/newAction',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                resp.render('deviceNewAction', {
                    device: await Device.findByUid(req.params.uid),
                    otherDevices: (await Device.findOthers(req.params.uid)).map((device: DeviceModel) => ({
                        uid: device.uid,
                        label: `${device.type} - ${device.location}`,
                    })),
                })
        );

        this.Routes.post(
            '/device/:uid/newAction',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                await Device.findByUid(req.params.uid)
                    .then((device: DeviceModel) => device.appendAction(new Action(req.body)))
                    .then(() => {
                        resp.redirect(`/device/${req.params.uid}/event`);
                    })
                    .catch(common.error(resp));
            }
        );

        this.Routes.get(
            '/device/:uid/popAction',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                await Device.findByUid(req.params.uid)
                    .then((device: DeviceModel) => device.popAction())
                    .then(() => {
                        resp.redirect(`/device/${req.params.uid}/event`);
                    })
                    .catch(common.error(resp));
            }
        );

        this.Routes.get(
            '/device/:uid/event',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                const device = await Device.findByUid(req.params.uid);
                resp.render('deviceEvents', { device });
            }
        );

        this.Routes.get(
            '/device/:uid/remove',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                Device.findByUidAndRemove(req.params.uid)
                    .then(() => resp.redirect('/devices'))
                    .catch((err: any) => {
                        console.error(err);
                        resp.redirect('/devices');
                    })
        );

        this.Routes.get(
            '/device/:uid/logs',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                const device = await Device.findByUid(req.params.uid);
                // FIXME: We will be able to remove the next line once the DB has been migrated
                if (!device.logs) device.logs = [];
                device.logs = device.logs instanceof Array ? device.logs : JSON.parse(device.logs);
                resp.render('deviceLogs', { device });
            }
        );

        // Route just access from the web page
        this.Routes.get(
            '/device/web/:location/unlock',
            async (req: any, resp: express.Response, next: express.NextFunction) => {
                // Mimic the mobile device as a device
                const deviceUid = `mobile-${req.params.location}`;

                // Give a proper response
                this.hacklandEvents
                    .handleEvent('web-user', deviceUid, req.user)
                    .then((authenticated) => {
                        if (authenticated && authenticated.isSuccess()) {
                            resp.render('success', {
                                title: 'Success',
                                message: 'The door is now unlocked',
                                uri: '/',
                            });
                        } else {
                            resp.render('accessDenied');
                        }
                    })
                    .catch(common.error(resp));
            }
        );

        //
        // API access methods
        //

        this.Routes.get('/api/devices', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            Device.getAll()
                .then((devices: any) => {
                    resp.json(devices);
                })
                .catch(common.error(resp))
        );

        this.Routes.put(
            '/api/device/:uid',
            (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                Device.findOneAndUpdate(
                    {
                        uid: req.params.uid,
                    },
                    req.body
                )
                    .then((device: any) => {
                        resp.json(device);
                    })
                    .catch(common.error(resp))
        );

        // Route just for mobile devices
        this.Routes.post(
            '/api/device/mobile/:location/:uid',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                // Mimic the mobile device as a device
                const deviceUid = `mobile-${req.params.location}`;
                const message = req.params.uid;

                // Give a proper response
                const authenticated = await this.hacklandEvents.handleEvent('card', deviceUid, message);
                if (authenticated && authenticated.isSuccess()) {
                    resp.json('Request sent');
                } else {
                    resp.status(403); // Forbidden
                    resp.end();
                }
            }
        );
    }
}

export default DeviceController;
