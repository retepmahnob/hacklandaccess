import { Router, Request, Response, NextFunction } from 'express';
require('dotenv').config();
import common from '../common';
import SlackModel from '../models/Slack';

export default class SlackController {
    public Routes = Router();
    slack: SlackModel;

    constructor(slackModel: SlackModel) {
        this.slack = slackModel;
        this.initialise();
    }

    private async initialise(): Promise<void> {
        this.Routes.get('/slack/channels', async (req: Request, resp: Response, next: NextFunction) =>
            this.slack
                .getSlackChannels()
                .then((channels) => resp.render('slackChannels', { channels }))
                .catch(common.error(resp))
        );

        this.Routes.get('/slack/channel/:channelId', async (req: Request, resp: Response, next: NextFunction) => {
            const channel = await this.slack.getChannelById(req.params.channelId);
            return this.slack
                .getMembersInChannel(req.params.channelId)
                .then((members) => resp.render('slackChannelMembers', { channel, members }))
                .catch(common.error(resp));
        });

        this.Routes.get('/slack/member/:slackUserId', async (req: Request, resp: Response, next: NextFunction) => {
            return this.slack
                .getMemberById(req.params.slackUserId)
                .then((slackUser) => resp.render('slackUser', { slackUser }))
                .catch(common.error(resp));
        });
    }
}
