/**
 * These are the routes where the user can administer their own profile.
 */
import * as express from 'express';
import Token, { TokenModel } from '../models/Token';
import common from '../common';

export class SelfController {
    public Routes = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        //
        // WWW access methods
        //
        this.Routes.get('/self/linktoken', async (req: any, resp: express.Response, next: express.NextFunction) => {
            resp.render('selfLinkToken', {
                tokens: await Token.getRecent(),
            });
        });

        this.Routes.get(
            '/self/linktoken/:tokenId',
            async (req: any, resp: express.Response, next: express.NextFunction) =>
                Token.get(req.params.tokenId)
                    .then((token: TokenModel) => token.assignUser(req.user.userId))
                    .then(() => resp.redirect('/self/tokens'))
                    .catch(common.error(resp))
        );

        this.Routes.get(
            '/self/unlinktoken/:tokenId',
            async (req: any, resp: express.Response, next: express.NextFunction) =>
                Token.get(req.params.tokenId)
                    .then((token: TokenModel) => token.clearUser())
                    .then(() => resp.redirect('/self/tokens'))
                    .catch(common.error(resp))
        );

        this.Routes.get('/self/tokens', async (req: any, resp: express.Response, next: express.NextFunction) =>
            resp.render('selfTokens', { tokens: await Token.getForUser(req.user.userId) })
        );
    }
}
export default SelfController;
