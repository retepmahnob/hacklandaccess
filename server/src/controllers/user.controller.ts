import * as express from 'express';
import User, { UserModel } from '../models/User';
import SlackModel from '../models/Slack';
import common from '../common';
import Config from '../configuration';

export class UserController {
    public Routes = express.Router();
    private slack: SlackModel;

    constructor(slack: SlackModel) {
        this.slack = slack;
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        //
        // WWW access methods
        //

        this.Routes.get('/users', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('users', { users: await User.getAll() })
        );

        // We can pass the `name` and `slackUserId` to this query.
        this.Routes.get('/user/new', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('userAdd', { user: req.query })
        );

        this.Routes.get(
            '/user/:userId/edit',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                resp.render('userEdit', { user: await User.findByUserId(req.params.userId) })
        );

        this.Routes.get(
            '/user/:userId/remove',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                User.findByUserIdAndRemove(req.params.userId)
                    .then(() => resp.redirect('/users'))
                    .catch((err: any) => {
                        console.error(err);
                        resp.redirect('/users');
                    })
        );

        this.Routes.post(
            '/user/:userId/edit',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                req.body.admin = req.body.admin === 'on';
                req.body.enabled = req.body.enabled === 'on';
                await User.updateByUserId(req.params.userId, req.body)
                    .then((user: any) =>
                        resp.render('success', {
                            title: 'Updated user',
                            message: user.name,
                            uri: '/users',
                        })
                    )
                    .catch(common.error(resp));
            }
        );

        this.Routes.post(
            '/user/new',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                await User.addNew(req.body)
                    .then((user: any) =>
                        resp.render('success', {
                            title: 'Created new user',
                            message: user.name,
                            uri: '/users',
                        })
                    )
                    .catch(common.error(resp))
        );

        this.Routes.get(
            '/user/:userId/slacklink',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                await User.findByUserId(req.params.userId)
                    .then(async (user: any) =>
                        resp.render('userSlackLink', {
                            user,
                            slackMembers: await this.slack.getMembersInChannel(Config.slackSupportersChannelId),
                        })
                    )
                    .catch(common.error(resp))
        );

        this.Routes.get(
            '/user/:userId/slacklink/:slackUserId',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                await User.findByUserId(req.params.userId)
                    .then(async (user: any) => {
                        const slackId = req.params.slackUserId == 'null' ? null : req.params.slackUserId;
                        await user.setSlackUserId(slackId);

                        // TODO: Sync the user with the slack member here.
                    })
                    .then(() => resp.redirect('/users'))
                    .catch(common.error(resp))
        );

        //
        // API access methos
        //

        this.Routes.get('/api/users', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            User.getAll()
                .then((users: any) => {
                    resp.json(users);
                })
                .catch(common.error(resp))
        );

        this.Routes.get(
            '/api/user/:userId',
            (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                User.findByUserId(req.params.userId)
                    .then((user: any) => resp.json(user))
                    .catch(common.error(resp))
        );

        // Add a new user
        this.Routes.post('/api/user', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            User.addNew(req.body)
                .then((user: any) => resp.json(user))
                .catch(common.error(resp))
        );

        // Delete an existing users
        this.Routes.delete(
            '/api/user/:userId',
            (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                User.findOneAndDelete({ userId: req.params.userId })
                    .then((user: any) => resp.json(user))
                    .catch(common.error(resp))
        );
    }
}
export default UserController;
