import * as express from 'express';
import AccessLog from '../models/AccessLog';

export class AccessLogController {
    public Routes = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        //
        // WWW access methods
        //

        this.Routes.get('/log', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('accessLog', {
                accessLog: await AccessLog.getAll(req.query.limit),
            })
        );
    }
}
export default AccessLogController;
