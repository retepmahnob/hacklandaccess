import Device, { DeviceModel } from '../models/Device';
import Action from '../models/Action';
import PubSub from '../pubsub';
import * as Sentry from '@sentry/node';

const WAIT_TIME_MIN = 30;

async function getMatchingReader(door: DeviceModel) {
    // Find the matching device
    let devs = await Device.getTypeAndLocation('rfid-reader', door.location);
    if (devs.length != 1 && !devs[0].enabled) {
        console.error('devs: ', devs);
        throw new Error('Unable to find one lock');
    }
    return devs[0]; // rfidReader
}

function isNightTime(): boolean {
    const d = new Date();
    return d.getHours() >= 20 && d.getHours() <= 6;
}

export class StateController {
    private lastAlert = new Date(0).toISOString();

    constructor() {
        setInterval(async () => {
            await this.monitor();
        }, 1000);
    }

    private async monitor() {
        const time_past = new Date(Date.now() - WAIT_TIME_MIN * 60 * 1000).toISOString();
        let doors = await Device.getAllOfType('lock');
        for (let door of doors) {
            if (door.state != 'held-open') continue;
            if (time_past < door.stateChangeTime) continue;
            if (time_past < this.lastAlert) continue;
            if (!isNightTime()) continue;

            // Send the alert to Sentry, it will work out the notifications.
            this.lastAlert = new Date().toISOString(); // now
            const msg = `DOOR_OPEN: ${door.location}`;
            console.error(msg);
            Sentry.captureMessage(msg);
        }
    }

    static async handleStateChange(deviceId: string, newState: string): Promise<void> {
        let device = await Device.findByUid(deviceId);
        await device.setState(newState);
        if (!device || device.type !== 'lock') return;
        let rfidReader = await getMatchingReader(device);
        let action = Action.buzzerOff(rfidReader.uid);
        switch (newState) {
            case 'open':
            case 'closed': {
                break;
            }
            case 'held-open': {
                if (!isNightTime()) break;
                action = Action.buzzerOn(rfidReader.uid);
                break;
            }
            default: {
                throw new Error(`Unexpected state value: ${newState}`);
            }
        }
        await PubSub.emitMQTTMessage(action);
    }
}
export default StateController;
