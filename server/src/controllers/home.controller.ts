import * as express from 'express';
import PubSub from '../pubsub';
import common from '../common';
import Device from '../models/Device';

export class HomeController {
    public Routes = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    private initialiseRoutes() {
        this.Routes.get('/', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('index', {
                locks: await Device.getAllOfType('lock'),
            })
        );

        this.Routes.get('/mqtt/publish', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('mqttPublish', {
                topic: '/device/[UID]/auth',
                message: 'fail',
            })
        );

        this.Routes.post('/mqtt/publish', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            PubSub.pub(req.body.topic, req.body.message)
                .then(() => {
                    resp.render('mqttPublish', req.body);
                })
                .catch(common.error(resp))
        );
    }
}

export default HomeController;
