import * as express from 'express';
import Token, { TokenModel } from '../models/Token';
import User from '../models/User';
import common from '../common';

export class TokenController {
    public Routes = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        //
        // WWW access methods
        //

        this.Routes.get('/tokens', async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('tokens', { tokens: await Token.getAll(req.query.limit) })
        );

        this.Routes.get(
            '/token/:tokenId/link',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                resp.render('tokenLink', {
                    token: req.params.tokenId,
                    users: await User.getAll(),
                });
            }
        );

        this.Routes.get(
            '/token/:tokenId/unlink',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) => {
                Token.get(req.params.tokenId)
                    .then(async (token: TokenModel) => await token.clearUser())
                    .then(async () => resp.redirect('/tokens'))
                    .catch(common.error(resp));
            }
        );

        // FIXME: This should really be a post, but the pug code is a bit basic at the moment.
        this.Routes.get(
            '/token/:tokenId/link/:userId',
            async (req: express.Request, resp: express.Response, next: express.NextFunction) =>
                Token.get(req.params.tokenId)
                    .then((token: TokenModel) => token.assignUser(req.params.userId))
                    .then(() => resp.redirect('/tokens'))
                    .catch(common.error(resp))
        );
    }
}
export default TokenController;
