import * as express from 'express';

const SlackStrategy = require('passport-slack').Strategy;
import passport_ from 'passport';
import Config from '../configuration';
import User, { UserModel } from '../models/User';

// All users have access to the following
const PUBLIC_ROUTES = ['/login', '/logout', '/auth/slack', '/auth/slack/callback', /^\/api\/device\/mobile\/.*/];

// Non-admin users have access to the following
const NON_ADMIN_ROUTES = ['/', /^\/self.*/, /^\/device.*/];

function isAuthorisedRoute(routeList: Array<string | RegExp>, currentRoute: string) {
    for (const i in routeList) {
        const authedRoute = routeList[i];
        if (authedRoute === currentRoute) return true;
        if (typeof authedRoute == 'object' && currentRoute.match(authedRoute)) return true;
    }
    return false;
}

export default class AuthController {
    public Routes = express.Router();
    public passport = passport_;

    constructor() {
        this.initialisePassportJs();
        this.initialiseRoutes();
    }

    private initialisePassportJs() {
        // Configure passport-slack
        this.passport.use(
            new SlackStrategy(
                {
                    clientID: Config.slackClientId,
                    clientSecret: Config.slackClientSecret,
                    callbackURL: `http://${Config.slackCallbackDomain}/auth/slack/callback`,
                    skipUserProfile: false,
                    scope: ['identity.basic'],
                },
                (accessToken: any, refreshToken: any, profile: any, done: any) => {
                    // optionally persist profile data
                    done(null, profile);
                }
            )
        );

        // These get called for each slack authentication, it then passes
        // the details to the deserialiser.
        this.passport.serializeUser((user: any, done: Function) => {
            done(null, user.id);
        });

        // This gets call on every access request
        this.passport.deserializeUser(async (slackUserId: string, done: Function) => {
            await User.findBySlackId(slackUserId)
                .then((user: UserModel) => done(null, user))
                .catch((err: any) => done(err, null));
        });
    }

    // Authenticate only those users which are admin users
    public authenticateRouteMiddleware() {
        return (req: any, resp: express.Response, next: express.NextFunction) => {
            // if (true) return next();
            if (isAuthorisedRoute(PUBLIC_ROUTES, req.path)) {
                return next();
            }
            if (req.isAuthenticated()) {
                if (req.user.admin === true) return next();
                if (isAuthorisedRoute(NON_ADMIN_ROUTES, req.path)) return next();

                // Admin only
                console.log(`Admin Access Denied to: ${req.path}, user=${req.user}`);
                resp.render('accessDenied', { message: 'You need to be an administrator to access this page.' });
                return;
            }

            // Not authorised
            console.log(`Access Denied to: ${req.path}, user=${req.user}`);
            resp.render('accessDenied', {
                message:
                    'You are not a member of the Supports channel within the Hackland organisation on Slack, or you are a very recent member, if so try again in a few hours.',
            });
        };
    }

    // Return values to the render engine to see if the user is logged in, an admin, etc
    public renderRequestMiddleware() {
        return (req: any, res: any, next: Function) => {
            res.locals.auth = {
                admin: req.user && req.user.admin ? true : false,
                loggedIn: typeof req.user === 'object',
                isMember: req.user && req.user.enabled,
                user: req.user
                    ? {
                          realName: req.user.name,
                          memberStatus: req.user.memberStatus(),
                      }
                    : {},
            };
            next();
        };
    }

    private initialiseRoutes(): void {
        this.Routes.get('/login', (req: express.Request, resp: express.Response, next: express.NextFunction) =>
            resp.render('login')
        );

        this.Routes.get('/logout', (req: express.Request, resp: express.Response, next: express.NextFunction) => {
            req.logout();
            resp.redirect('/login');
        });

        // path to start the OAuth flow
        this.Routes.get('/auth/slack', this.passport.authenticate('slack'));

        // OAuth callback url
        this.Routes.get(
            '/auth/slack/callback',
            this.passport.authenticate('slack', { failureRedirect: '/login' }),
            (req, res) => {
                res.redirect('/');
            }
        );
    }
}
