function error(resp: any): any {
    return (err: any) => {
        console.error(`Caught error:${err}`);
        resp.status(500);
        resp.end();
    };
}

function time(): string {
    return `${new Date().toISOString()}`;
}

function localDate(datetime: Date): string {
    const pad = (n: number) => (n < 10 ? '0' + n : n);
    const d = new Date(datetime);

    return `${d.getFullYear()}-${pad(d.getMonth() + 1)}-${pad(d.getDate())}  ${d.toLocaleTimeString()}`;
}

export default { error, time, localDate };
