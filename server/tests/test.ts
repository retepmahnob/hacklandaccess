import mqtt from 'mqtt';
import Config from '../src/configuration';
import test from 'tape';

//
// Set up the test suite
//

const mqttClient = mqtt.connect(Config.mqttConnectString);

function mqttTest(a: any): Promise<void> {
    const expectedResponses = [{ m: a.message, t: a.topic }, ...a.expectedResponses];
    expectedResponses.forEach((a) => (a.trig = 0));
    // console.log(expectedResponses);

    function send() {
        mqttClient.publish(a.topic, a.message);
    }

    function countMessage(t: string, m: string) {
        let count = 0;
        expectedResponses.forEach((b) => {
            if (b.t === t && b.m === m) {
                b.trig += 1;
                count += 1;
            }
        });
        if (count === 0) {
            expectedResponses.push({ t, m, trig: -1, failure: 'not captured' });
        }
    }

    return new Promise((resolve, reject) =>
        test(a.name, function (t: any) {
            t.plan(2);

            const fn = (topic: any, message: any) => {
                countMessage(topic, message.toString());
            };
            mqttClient.on('message', fn);
            send();

            setTimeout(() => {
                mqttClient.removeListener('message', fn);
                const good = expectedResponses.filter((b) => b.trig !== 1).length == 0;
                if (!good) console.log(expectedResponses);
                t.ok(good, 'All responses were valid');
                t.pass('No lingering messages');
                resolve();
            }, 300);
        })
    );
}

//
// Do the actual tests
//

mqttClient.on('connect', () => {
    mqttClient.subscribe('/device/#');

    const RFID_READER = '84F3EB770B19';
    const LOCK = '600194202C6E';
    mqttTest({
        name: 'Register rfid-reader',
        topic: `/device/${RFID_READER}/register`,
        message: 'rfid-reader',
        expectedResponses: [],
    })
        .then(() =>
            mqttTest({
                name: 'Invalid event',
                topic: `/device/${RFID_READER}/blahblah`,
                message: 'rfid-reader',
                expectedResponses: [],
            })
        )
        .then(() =>
            mqttTest({
                name: 'Invalid card - expect a failure response',
                topic: `/device/${RFID_READER}/card`,
                message: 'invalid card',
                expectedResponses: [{ t: `/device/${RFID_READER}/auth`, m: 'failure' }],
            })
        )
        .then(() =>
            mqttTest({
                name: 'Valid card - linked to a non-slack user',
                topic: `/device/${RFID_READER}/card`,
                message: '30A0CD5D',
                expectedResponses: [
                    { t: `/device/${RFID_READER}/auth`, m: 'success' },
                    { t: `/device/${LOCK}/unlock`, m: '5' },
                ],
            })
        )
        .then(() =>
            mqttTest({
                name: 'Valid card - but not registered to a user',
                topic: `/device/${RFID_READER}/card`,
                message: 'DD6F9E29',
                expectedResponses: [{ t: `/device/${RFID_READER}/auth`, m: 'failure' }],
            })
        )
        .then(() =>
            mqttTest({
                name: 'Valid card - linked a user who is deactivated',
                topic: `/device/${RFID_READER}/card`,
                message: 'DD6F9E2',
                expectedResponses: [{ t: `/device/${RFID_READER}/auth`, m: 'failure' }],
            })
        )
        .then(() => mqttClient.end());
});
