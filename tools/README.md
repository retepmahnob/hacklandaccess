# Tools

Basic tools get get door running.

## Get users

```sh
curl --silent localhost:8088/api/users | python -m json.tool
```

## Create a new user

1. Edit the `user.json` file.
1. Run the following command
1. Make sure the user is added using the "Get users" command above.

```sh
curl -iX POST localhost:8088/api/user -d @user.json --header "Content-Type: application/json"
```

## Delete a user

```sh
curl -iX DELETE localhost:8088/api/user/Cameron.Stewart@gmail.com  --header "Content-Type: application/json"
```

## List all devices

```sh
curl --silent localhost:8088/api/devices | python -m json.tool
```

## Update a device's location

Update the `DEVICE_UID` and `LOCATION` below then run the code below.

```sh
DEVICE_UID="600194202C80"
LOCATION="HL1_DOOR"
curl -iX PUT localhost:8088/api/device/${DEVICE_UID} -d "{\"location\":\"${LOCATION}\"}" --header "Content-Type: application/json"
```
